(TeX-add-style-hook
 "tcirc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "pgfplots"
    "tkz-euclide"
    "mathrsfs")
   (TeX-add-symbols
    "degre"))
 :latex)

