(TeX-add-style-hook
 "5L_global"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "12pt" "final")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontspec" "no-math") ("babel" "italian") ("xcolor" "svgnames") ("hyperref" "colorlinks=true" "linkcolor=DarkSlateBlue")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir12"
    "inputenc"
    "fontspec"
    "babel"
    "microtype"
    "amsmath"
    "amssymb"
    "amsthm"
    "bm"
    "manfnt"
    "mathtools"
    "enumitem"
    "siunitx"
    "xcolor"
    "hyperref"
    "tikz"
    "tkz-euclide"
    "subcaption"
    "pgfplots")
   (TeX-add-symbols
    '("hint" 1)
    '("pd" 1)
    "wideparen"
    "e"
    "R")
   (LaTeX-add-labels
    "fig:ivt"
    "exc:ivt")
   (LaTeX-add-amsthm-newtheorems
    "exc"
    "eg"
    "thm"
    "cor"
    "prp"
    "dfn"
    "rmk")
   (LaTeX-add-amsthm-newtheoremstyles
    "rmk"
    "thm"
    "dfn")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("norm" "")))
 :latex)

