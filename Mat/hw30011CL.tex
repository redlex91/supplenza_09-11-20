\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}


\DeclareFontFamily{OMX}{yhex}{}
\DeclareFontShape{OMX}{yhex}{m}{n}{<->yhcmex10}{}
\DeclareSymbolFont{yhlargesymbols}{OMX}{yhex}{m}{n}
\DeclareMathAccent{\wideparen}{\mathord}{yhlargesymbols}{"F3}


\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother

\usepackage{enumitem}
\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}


\usepackage{tikz,tkz-euclide,subcaption}
\usetikzlibrary{arrows}
\newcommand{\sln}[1]{\hfill\color{DarkSlateBlue}$\mathcal{S}=#1$}
\newcommand{\slnn}[1]{\hfill\color{DarkSlateBlue}$#1$}

\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{rmk}
\newtheorem{exc}{Esercizio}

\newtheoremstyle{thm}
{\topsep}
{\topsep}
{\itshape}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{thm}
\newtheorem{thm}{Teorema}[chapter]
\theoremstyle{thm}
\newtheorem{cor}{Corollario}[chapter]
\theoremstyle{thm}
\newtheorem{prp}{Proposizione}[chapter]


\newtheoremstyle{dfn}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{dfn}
\newtheorem{dfn}{Definizione}[chapter]

\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{section}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\title{Esercizi di Algebra per la 1CL}
\author{Alessandro Rossi}

\begin{document}
\maketitle

La forma generale di una equazione di primo grado in una incognita $x$ (o equazione lineare in $x$) è
\[
ax-b=0,\quad a,b\in\mathbb{R}.
\]
Abbiamo visto in classe che possono presentarsi tre possibilità:
\begin{enumerate}
\item $a\ne0$, allora l'equazione ha \emph{una ed una sola} soluzione $x=\frac{b}{a}$, l'insieme delle soluzioni è un \emph{singoletto}: $\mathcal{S}=\lbrace\frac{b}{a}\rbrace$ e l'equazione è \emph{determinata};
\item $a=0\land b\ne0$ (si legge ``$a=0$ e $b\ne0$''), allora l'equazione non ha soluzioni: l'insieme delle soluzioni è vuoto, $\mathcal{S}=\emptyset$, e l'equazione è \emph{impossibile};
\item se $a=0\land b=0$, allora l'equazione ha \emph{infinite} soluzioni: l'insieme delle soluzioni è $\mathcal{S}=\mathbb{R}$ e l'equazione è \emph{indeterminata}.
\end{enumerate}
Notare che i due casi in cui $a=0$ sono comunque considerati equazioni di primo grado nonostante il \emph{grado} del polinomio che compare nella forma normale sia nullo (è un numero reale).

\begin{quote}\sffamily
Portare ciascuna delle seguenti equazioni in \emph{forma normale}: individuare il polinomio $P(x)$ che compare nella forma normale (possibilmente scrivendolo in modo che i coefficienti siano interi, cioè non frazionari, e non abbiano fattori comuni) e determinare il grado dell'equazione. Per le \textbf{sole} equazioni di primo grado, determinare le soluzioni dell'equazione e classificarla, ovvero dire se è \emph{determinata, indeterminata o impossibile}. Le prime quattro sono risolte.
\end{quote}
\begin{exc}
\item \[\frac19(x+3)^3=(x+1)^2-\frac13\]
\end{exc}
\begin{proof}[Esempio di svolgimento]
Portiamo tutti i denominatori a $9$
\[
\frac{(x+3)^2}{9}=\frac{9(x+1)^2}{9}-\frac{3}{9}
\]
quindi moltiplichiamo entrambi i membri per $9$, ottenendo
\[
	(x+3)^2=9(x+1)^2-3
\]
sviluppiamo i prodotti notevoli
\[
x^3+9x^2+27x+27=9x^2+18x+9-3
\]
ed infine portiamo l'equazione in forma normale
\[
x^3+9x+21=0.
\]
Il polinomio che compare nella forma normale è
\[
P(x)=x^3+9x+21,
\]
quindi il grado dell'equazione è $3$.
\end{proof}
\begin{exc}
 \[\frac{(3x-1)^2}{15} +\frac{x}{2}=\frac{3-x}{5}+\frac{3x^2}{5}\]
\end{exc}
\begin{proof}[Esempio di svolgimento]
Il mcm dei denominatori è $30$, quindi portiamo tutti i denominatori a $30$
\[
\frac{2(3x-1)^2}{30}+\frac{15x}{30}=\frac{6(3-x)}{30}+\frac{18x^2}{30}
\]
moltiplichiamo entrambi i membri dell'equazione per $30$ in modo da liberarci dei denominatori
\[
18x^2-12x+2+15x=18-6x+18x^2
\]
e portiamo in forma normale
\[
9x-16=0.
\]
Il polinomio che compare nella forma normale dell'equazione è
\[
P(x)=9x-16,
\]
quindi l'equazione è di primo grado, ovvero lineare in $x$, e l'unica soluzione è
\[
x=\frac{16}{9},
\]
pertanto l'equazione è \emph{determinata}: $\mathcal{S}=\lbrace\frac{16}{9}\rbrace$.
\end{proof}
\begin{exc}
\[3(x+1)-2x+2(x-3)=3x+1\]
\end{exc}
\begin{proof}[Esempio di svolgimento]
Per prima cosa eseguiamo i prodotti
\[
3x+3-2x+2x-6=3x+1
\]
quindi portiamo in forma normale
\[
0\cdot x-4=0.
\]
Il polinomio
\[
P(x)=0\cdot x-4=-4
\]
ha grado $0$, l'equazione è di grado $1$ (degenere) e non ammette soluzioni: infatti nessun numero moltiplicato per $0$ dà $4$. L'equazione è pertanto \emph{impossibile}: $\mathcal{S}=\emptyset$.
\end{proof}
\begin{exc}
 \[2x(x-3)+3=2x(x+2)-5(2x-\frac45)-1\]
\end{exc}
\begin{proof}[Esempio di svolgimento]
Per prima cosa calcoliamo i prodotti
\[
2x^2-6x+3=2x^2+4x-10x+4-1
\]
quindi portiamo in forma normale
\[
0\cdot x-0=0.
\]
Il polinomio
\[
P(x)=0\cdot x-0=0
\]
ha grado $0$, l'equazione è di grado $1$ (degenere) e ammette infinite soluzioni: infatti qualunque numero moltiplicato per $0$ dà come risultato $0$. L'equazione è \emph{indeterminata}: $\mathcal{S}=\mathbb{R}$.
\end{proof}
\begin{exc}
\[
9\Big(\frac{x}{3}-1\Big)^3+\frac{3x-2}{15}-\frac{x^3}{3}=\frac{(5^{-10})^3}{(\frac15)^{18}(\frac15)^{11}}-3x^2-\frac45 x
\]
\sln{\{\frac{14}{15}\}}
\end{exc}
\begin{exc}
\[
(-2x+1)(2x+1)-(x-1)^3-\frac32 x=-(x+3)^2+\frac32x-x^3+11
\]\sln{\mathbb{R}}
\end{exc}
\begin{exc}
\[
\frac{(x-2)^3}{2}+3x^2=6x-\frac72
\]\slnn{P(x)=x^3-1,\text{grado }3}
\end{exc}
\begin{exc}\[
\frac{(3x-1)^2}{6}+\frac{7}{2}x-\frac{4}{3}=\frac{(x+5)(x+10)}{6}+\frac{4}{3}x^2
\]\sln{\emptyset}\end{exc}
\begin{exc}
\[
\Big(\frac{x}{2}-\frac13\Big)\Big(\frac{x}{3}+\frac12\Big)-\Big[(x+2)(x-2)-(x-2)^2+8\Big]^2\cdot\frac{(\frac{1}{2})^5}{3}=-\frac16\]
\sln{\lbrace0\rbrace}
\end{exc}
\begin{exc}\[
\Big(\frac{x}{5}-1\Big)\Big(\frac{x}{5}+1\Big)-\frac{(x+2)^2}{5}=-\frac{(2x-5)^2}{25}
\]\sln{\{-\frac12\}}\end{exc}
\begin{exc}\[
\frac{\Big[(x+2)^2+(x-1)(-x-1)-2x\Big]^2}{6}=\frac{(1+2x)^2}{6}+\frac83x\]
\sln{\emptyset}\end{exc}
\begin{exc}\[
\frac{7x-18}{4}=\frac{(x+2)(2x-5)}{4}-\frac{(x-2)^2}{2}\]\sln{\mathbb{R}}
\end{exc}
\begin{exc}
\[(2x+3)^2-(x+3)^2=(x+1)^3-(x-1)(x^2+x+1)
\]\sln{\{\frac23\}}\end{exc}
\begin{exc}
\[\Big[(2 x + 1)^2 - 2 (x + 2)^2\Big]^2 = (2 x^2 - 7)^2\]
\slnn{P(x)=2x^3-2x^2-7x,\text{grado 3}}
\end{exc}
\begin{exc}
\[-\frac{37}{20}x+\frac{(x+1)^3}{5}-\frac{3}{10}=5x\Big(\frac{x}{5}-\frac12\Big)\Big(\frac{x}{5}+\frac12\Big)+\frac35x^2\]
\sln{\emptyset}
\end{exc}
\end{document}
