\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm,manfnt}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}


\DeclareFontFamily{OMX}{yhex}{}
\DeclareFontShape{OMX}{yhex}{m}{n}{<->yhcmex10}{}
\DeclareSymbolFont{yhlargesymbols}{OMX}{yhex}{m}{n}
\DeclareMathAccent{\wideparen}{\mathord}{yhlargesymbols}{"F3}


\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother

\usepackage{enumitem}
\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}


\usepackage{tikz,tkz-euclide,subcaption}
\captionsetup[sub]{font=tiny}
\captionsetup{font=scriptsize}
\usetikzlibrary{arrows}
\usepackage{pgfplots}
\pgfplotsset{compat=1.17}


\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{rmk}
\newtheorem{exc}{Esercizio}
\theoremstyle{remark}
\newtheorem{eg}{Esempio}
\newtheoremstyle{thm}
{\topsep}
{\topsep}
{\itshape}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{thm}
\newtheorem{thm}{Teorema}
\theoremstyle{thm}
\newtheorem{cor}{Corollario}[chapter]
\theoremstyle{thm}
\newtheorem{prp}{Proposizione}[chapter]


\newtheoremstyle{dfn}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{dfn}
\newtheorem{dfn}{Definizione}
\theoremstyle{remark}
\newtheorem{rmk}{Osservazione}

\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}
\newcommand{\e}{\textsf{e}}
\newcommand{\hint}[1]{\begin{quote}\itshape Suggerimento. #1\end{quote}}
\setsecnumdepth{section}
\counterwithout{section}{chapter}
\counterwithout{figure}{chapter}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\newcommand{\R}{\mathbb{R}}
\title{Proprietà globali delle funzioni continue}
\author{Alessandro Rossi}

\begin{document}
\maketitle
Sia $f\colon X\to Y$ una funzione tra due insiemi, chiamiamo \emph{immagine} di $f$ il sottoinsieme del codominio $Y$
\[
  f(X)\coloneqq\lbrace y\in Y\mid y=f(x)\text{ per qualche }x\in X\rbrace.
\]
L'immagine di una funzione è detto anche \emph{range}: una funzione la cui immagine coincide col codominio è detta \emph{suriettiva} o \emph{epi} (o, in inglese, \emph{onto}).

Per funzioni reali di una variabile reale, l'immagine è un sottoinsieme della retta dei numeri reali.
\begin{dfn}
  Sia $f\colon D\subset\R\to\R$ una funzione, diciamo che $f$ possiede \emph{massimo globale} se la sua immagine è superiormente limitata e $M\in f(D)$ è il massimo dell'immagine
  \[
    \exists M\in f(D) : f(x)\leq M,\quad \forall x\in D,
  \]
  diciamo che $f$ possiede \emph{minimo globale} se la sua immagine è inferiormente limitata e $m$ è il suo minimo
  \[
    \exists m\in f(D):f(x)\geq m,\quad \forall x\in D.
  \]
\end{dfn}
È fondamentale che il massimo ed il minimo di un sottoinsieme siano essi stessi punti del sottoinsieme. Ad esempio, se consideriamo il sottoinsieme $[0,3)\subseteq\R$ il numero reale $3$ soddisfa la condizione $x\leq 3,\,\forall x\in[0,3)$ ma non è un massimo di $[0,3)$ poiché non appartiene al sottoinsieme; al contrario $0$ è \emph{il minimo} dell'insieme in quando soddisfa $x\geq 0,\,\forall x\in [0,3)$ e appartiene all'insieme stesso.

\begin{figure}[t]\centering
  \begin{subfigure}[t]{.45\textwidth}
    \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
      \begin{scriptsize}
        \begin{axis}[
          x=1cm,y=3cm,
          axis lines=middle,
          xmin=-3,
          xmax=3.5,
          ymin=-.5,
          ymax=.7,
          ticks=none,
          xlabel=$x$,ylabel=$y$
          ]
          \clip(-3,-.75) rectangle (3,.75);
          \addplot[domain=-5:-1,samples=501,smooth, line width=1pt, color=DarkSlateBlue] {sqrt((\x)^(2)-1)/(\x)^(3)};
          \addplot[domain=1:5,samples=500,smooth, line width=1pt, color=DarkSlateBlue] {sqrt((\x)^(2)-1)/(\x)^(3)};
          \addplot[domain=0:5, samples=500, smooth, line width=.5pt, color=Crimson, dash pattern = on3pt off3pt] {0.3849};
          \addplot[domain=-5:0, samples=500, smooth, line width=.5pt, color=Crimson, dash pattern = on3pt off3pt] {-0.3849};

          \draw (0,0.3849) node[left] {\scriptsize$\frac{2}{3\sqrt3}$};
          \draw (0,-0.3849) node[right] {$-\frac{2}{3\sqrt3}$};
          \draw[fill=DarkSlateBlue, draw=DarkSlateBlue] (-1,0) circle (.5pt) node {};
          \draw[fill=DarkSlateBlue, draw=DarkSlateBlue] (1,0) circle (.5pt) node {};
          \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (1.22,0) node[below] {$\frac{\sqrt6}{2}$} -- (1.22,.3849);
          \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (-1.22,0) node[above] {$-\frac{\sqrt6}{2}$} -- (-1.22,-.3849);
          \draw[fill=Crimson, draw=Crimson] (1.22,0.3849) circle (1pt) node[above] {};
          \draw[fill=Crimson,draw=Crimson] (-1.22,-0.3849) circle (1pt) node[below] {};
        \end{axis}
      \end{scriptsize}
    \end{tikzpicture}
    \caption{La funzione il cui grafico è rappresentato in figura è limitata sia inferiormente, sia superiormente, quindi è limitata. Il suo massimo è $2/(3\sqrt3)$ e il suo minimo è $-2/(3\sqrt3)$. La funzione ha due punti estremali: $x=\sqrt{6}/{2}$ è punto di massimo (globale), mentre $x=-\sqrt{6}/{2}$ è punto di minimo (globale).}
  \end{subfigure}\hfill
  \begin{subfigure}[t]{.45\textwidth}\centering
    \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
      \begin{scriptsize}
        \begin{axis}[
          x=1cm,y=3cm,
          axis lines=middle,
          xmin=-3,
          xmax=3.5,
          ymin=-.25,
          ymax=.75,
          ticks=none,
          xlabel=$x$,ylabel=$y$
          ]
          \clip(-3,-.75) rectangle (3.5,.75);
          \addplot[domain=-5:-1,samples=501,smooth, line width=1pt, color=DarkSlateBlue] {sqrt(-((\x)^(2)-1)/(\x)^(3))};
          \addplot[domain=0.1:1,samples=500,smooth, line width=1pt, color=DarkSlateBlue] {sqrt(-((\x)^(2)-1)/(\x)^(3))};
          \addplot[domain=-7:10, samples=500, smooth, line width = .5pt, dash pattern = on3pt off3pt, color=Crimson] {0};
          \draw[fill=Crimson,draw=Crimson] (-1,0) circle (1pt) node[below]{$-1$};
          \draw[fill=Crimson,draw=Crimson] (1,0) circle (1pt) node[below] {$+1$};
        \end{axis}
      \end{scriptsize}
    \end{tikzpicture}\caption{La funzione il cui grafico è rappresentato in figura è limitata inferiormente ma non superiormente ($x=0$ è asintoto verticale destro): il suo minimo è $0$ e viene assunto sia in $x=-1$ sia in $x=+1$: pertanto la funzione ha due punti di minimo (globali), $x=-1$ e $x=+1$.}
  \end{subfigure}\caption{Esempi di massimi, minimi e punti estremali.}
\end{figure}
I punti \emph{del dominio} nei quali la funzione assume i valori di massimo e minimo globali sono detti \emph{punti estremali} o \emph{punti estremanti} della funzione. Distinguiamo due tipi di punti estremali: i punti di minimo globale ed i punti di massimo globale. Esistono anche punti di estremo \emph{locale} che studierete più avanti, ma, se non specificato, si intende sempre punti estremali \emph{globali}.
\begin{dfn}
  Data una funzione $f\colon D\subseteq \R\to\R$, diciamo che $c\in D$ è un \emph{punto di massimo (globale)} e che la funzione \emph{assume} il suo massimo in $c$ se
  \[
    f(x)\leq f(c),\quad\forall x\in D,
  \]
  diciamo invece che $c\in D$ è un \emph{punto di minimo (globale)} e che la funzione assume il suo minimo in $c$ se
  \[
    f(x)\geq f(c),\quad\forall x\in D.
  \]
  Un punto $c\in D$ in cui sia vera l'una o l'altra condizione è detto \emph{punto estremale} o \emph{punto di estremo (globale)}.
\end{dfn}
\section{Proprietà globali delle funzioni continue}
Ricordiamo che, dati due numeri reali $a\leq b$, l'\emph{intervallo chiuso e limitato (o compatto)} di estremi $a$ e $b$ è il sottoinsieme della retta reale
\[
  \mathopen{[}a,b\mathclose{]}\coloneqq\lbrace x\in\R\mid a\leq x\leq b\rbrace.
\]
Gli intervalli hanno la fondamentale proprietà di essere \emph{convessi}, ovvero, scelti due qualunque punti $x,y$ di $[a,b]$ si verifica che
\[
  \lbrace x+t(y-x)\mid t\in[0,1]\rbrace\subseteq [a,b].
\]
\begin{figure}[t]\centering
  \begin{subfigure}[t]{.45\textwidth}\centering
    \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
      \begin{scriptsize}
        \begin{axis}[
          x=1cm,y=1.5cm,
          axis lines=middle,
          xmin=-2.5,
          xmax=2.5,
          ymin=-1.5,
          ymax=.5,
          ticks=none,
          xlabel=$x$,ylabel=$y$,]
          \clip(-3,-5) rectangle (3,1.5);
          \draw [samples=500,rotate around={-180:(0,0)},xshift=0cm,yshift=0cm,line width=1pt,draw=DarkSlateBlue,domain=-10:10)] plot (\x,{(\x)^2/2/0.5});
          \draw [samples=500,rotate around={-180:(0,0)},xshift=0cm,yshift=0cm,line width=.5pt,draw=Crimson,domain=-5:5),dash pattern=on3pt off3pt] plot (\x,{0});
          \draw[fill=Crimson,draw=Crimson] (0,0) circle (1pt);
        \end{axis}
      \end{scriptsize}
    \end{tikzpicture}
    \caption{In figura è rappresentato il grafico della funzione $f\colon\R\to\R,\,x\mapsto -x^2$: la funzione è superiormente limitata ed inferiormente illimitata, ha massimo $0$ e punto di massimo (globale) in $x=0$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.45\textwidth}\centering
    \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
      \begin{scriptsize}
        \begin{axis}[
          x=1cm,y=1.5cm,
          axis lines=middle,
          xmin=-2.5,
          xmax=2.5,
          ymin=-1.5,
          ymax=.5,
          ticks=none,
          xlabel=$x$,ylabel=$y$,]
          \clip(-3,-5) rectangle (3,1.5);
          \draw [samples=500,rotate around={-180:(0,0)},xshift=0cm,yshift=0cm,line width=1pt,draw=DarkSlateBlue,domain=-1:1)] plot (\x,{(\x)^2/2/0.5});
          \draw [samples=500,rotate around={-180:(0,0)},xshift=0cm,yshift=0cm,line width=.5pt,draw=Crimson,domain=-5:5),dash pattern=on3pt off3pt] plot (\x,{0});
          \draw [samples=500,rotate around={-180:(0,0)},xshift=0cm,yshift=0cm,line width=.5pt,draw=Crimson,domain=-5:5),dash pattern=on3pt off3pt] plot (\x,{1});
          \draw (0,-1) node[right,yshift=-5pt]{$-1$};
          \draw (1,0) node[above]{$+1$};
          \draw (-1,0) node[above]{$-1$};
          \draw[line width = .5pt,dash pattern = on3pt off3pt] ((-1,0) -- (-1,-1);
          \draw[line width = .5pt,dash pattern = on3pt off3pt] ((1,0) -- (1,-1);
          \draw[fill=Crimson,draw=Crimson] (0,0) circle (1pt);
          \draw[fill=Crimson,draw=Crimson] (1,-1) circle (1pt);
          \draw[fill=Crimson,draw=Crimson] (-1,-1) circle (1pt);
        \end{axis}
      \end{scriptsize}
    \end{tikzpicture}
    \caption{In figura è rappresentato il grafico della funzione $f\colon[-1,1]\subseteq\R\to\R,\,x\mapsto -x^2$: la funzione è superiormente limitata ed inferiormente limitata, ha massimo $0$ e punto di massimo (globale) in $x=0$, inoltre ha minimo $-1$ e due punti di minimo in $x=\pm1$.}
  \end{subfigure}\caption{Relazione fra punti estremali e dominio della funzione.}
\end{figure}
\begin{thm}[Weierstra\ss]
  Sia $f\colon [a,b]\subseteq\R\to\R$ una funzione \emph{continua}, allora $f$ assume massimo e minimo, ovvero esistono (almeno) due punti $x_m,x_M\in[a,b]$ tali che $x_m$ è punto di minimo e $x_M$ è punto di massimo; in particolare la funzione è limitata (ovvero la sua immagine è un insieme sia superiormente sia inferiormente limitato).
\end{thm}

Il teorema di Weierstra\ss~ci grantisce che il grafico di una funzione continua definita su un intervallo chiuso e limitato non possa ``fuggire'' all'infinito.
\begin{rmk}
  Il teorema di Weierstra\ss~può essere generalizzato a funzioni continue definite su \emph{sottoinsiemi} chiusi e limitati (ovvero compatti) di $\R$, ma siccome queste nozioni topologiche non vengono introdotte agli studenti delle superiori, il teorema viene formulato per il caso particolare in cui il dominio è un \emph{intervallo} chiuso e limitato di $\R$, ovvero un sottoinsieme \emph{convesso} (è una richiesta molto stringente). A differenza del teorema di Weierstra\ss, negli altri due teoremi che vedremo in seguito l'ipotesi che il dominio sia un \emph{intervallo} chiuso e limitato è di fondamentale importanza.
\end{rmk}
\begin{rmk}
  Il teorema di Weierstra\ss~ci dice che la funzione può assumere il massimo ed il minimo anche negli estremi dell'intervallo. Ovviamente, la funzione può avere più di due punti estremali.
\end{rmk}
\begin{thm}[dei valori intermedi]
  Sia $f\colon[a,b]\subseteq \R\to\R$ una funzione continua, allora $f$ assume qualunque valore dell'immagine almeno una volta, ovvero qualunque valore compreso fra massimo e minimo; detti $m$ e $M$ il minimo ed il massimo (globali) di $f$,
  \[
    \forall y: m\leq y\leq M\,\exists x\in[a,b]:f(x)=y.
  \]
  In particolare,
  \[
    f([a,b])=[m,M].
  \]
\end{thm}
\begin{rmk}

  L'essenza del teorema dei valori intermedi è che una funzione continua trasforma un intervallo (chiuso e limitato) in un intervallo (chiuso e limitato), eventualmente ridotto a un punto (quando minimo e massimo coincidono).
\end{rmk}
\begin{dfn}
  Sia $f\colon D\subseteq\R\to\R$ una funzione, diciamo che un punto $c\in D$ è \emph{uno zero} di $f$ se $f(c)=0$.
\end{dfn}
Il prossimo teorema afferma che sotto certe condizioni una funzione ammette sempre almeno uno zero.
\begin{figure}[b]\centering
  \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
    \begin{scriptsize}
      \begin{axis}[
        x=1cm,y=.1cm,
        axis lines=middle,
        xmin=-3.5,
        xmax=3.5,
        ymin=-27,
        ymax=27,
        ticks=none,
        xlabel=$x$,ylabel=$y$
        ]
        \clip(-3.5,-27) rectangle (3.5,27);
        \draw (2,0) node[below]{$2$};
        \draw (-2,0) node[above]{$-2$};
        \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (-2,-17.8885) -- (-2,0);
        \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (2,17.8885) -- (2,0);
        \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (0,17.8885) -- (2,17.8885);
        \draw[draw=black, line width=.5pt, dash pattern= on 3pt off 3pt] (0,-17.8885) -- (-2,-17.8885);
        \draw[color=SeaGreen,line width=3pt,opacity=0.25] (-2,0)--(2,0);
\addplot[domain=-3:3,samples=101,smooth, line width=1pt, color=DarkSlateBlue] {(sqrt(9-x^2)*(x)^3)};
\draw[fill=SeaGreen, draw=SeaGreen] (-2,-17.8885) circle (1pt) node {};
\draw[fill=SeaGreen, draw=SeaGreen] (2,17.885) circle (1pt) node {};
\draw[fill=Crimson,draw=Crimson] (0,0) circle (1pt);
\draw[fill=Crimson,draw=Crimson] (-3,0) circle (1pt) node[above]{$-3$};
\draw[fill=Crimson,draw=Crimson] (3,0) circle (1pt) node[below] {$+3$};
\draw[fill=Crimson,draw=Crimson] (0,0) circle (1pt);
\draw (0,17.8885) node[left]{$8\sqrt5$};
\draw (0,-17.8885) node[right]{$-8\sqrt5$};
\end{axis}
\end{scriptsize}
\end{tikzpicture}\caption{La funzione $f$ il cui grafico è rappresentato in figura non soddisfa le ipotesi del teorema di Bolzano sul dominio $[-3,3]$, le soddisfa invece la sua restrizione $f|_{[-2,2]}$: pertanto la restrizione si deve annullare in un punto dell'intervallo aperto $(-2,2)$, ed infatti si annulla in $x=0$.}
\end{figure}
\begin{thm}[di Bolzano, o degli zeri]
  Sia $f\colon[a,b]\subseteq\R\to\R$ una funzione continua tale che $f(a)f(b)<0$, allora esiste $c\in(a,b)$ tale che $f(c)=0$, ovvero la funzione possiede (almeno) uno zero nell'interno dell'intervallo di definizione.
\end{thm}
\begin{rmk}
  Tanto il teorema di Bolzano, tanto quello dei valori intermedi hanno un'ipotesi ``nascosta'', ovvero che il dominio sia un intervallo chiuso e limitato di numeri \emph{reali}, infatti la dimostrazione dei teoremi fa uso della proprietà di completezza dei numeri reali (\emph{assioma di Dedekind} o \emph{least-upper bound property}):
  \begin{quote}
    Ogni sottoinsieme non vuoto di $\R$ superiormente limitato possiede un estremo superiore in $\R$.
  \end{quote}
  Ad esempio, la funzione
  \[
    f\colon[0,2]\subseteq \mathbb{Q}\to \R,\quad x\mapsto x^2-2
  \]
  è continua su $[0,2]$ e $f(0)=-2<0<f(2)=2$, tuttavia non si annulla mai su tale intervallo poiché il suo zero, $x=\sqrt2$, non è un numero razionale.
\end{rmk}
\begin{rmk}
Il teorema di Bolzano ha un'interpretazione grafica molto semplice: il grafico di una funzione continua definita su un intervallo chiuso e limitato di $\R$ deve attraversare l'asse $x$ per passare dal semipiano positivo a quello negativo e viceversa.
\end{rmk}
\begin{rmk}
Il teorema di Bolzano non ci dice quanti zeri ha la funzione su un intervallo, né ci dice come trovarli.
\end{rmk}
\section{Algoritmo dicotomico (o metodo di bisezione)}
Il teorema di Bolzano ci assicura che una funzione continua definita su un intervallo compatto possiede sicuramente almeno uno zero, ma non ci dice né quanti sono, né come trovarli. Tuttavia, applicando ripetutamente il teorema a restrizioni della funzione su intervalli via via più piccoli, possiamo provare a scovare questi zeri. L'algoritmo si compone di due passi:
\begin{description}
\item[0] definiamo
  \[
    a_0\coloneqq a,\,b_0\coloneqq b,\,c_0\coloneqq\frac{a_0+b_0}{2}
  \]
  se la funzione verifica le ipotesi del teorema di Bolzano sull'intervallo $[a_0,b_0]$, allora sappiamo che la funzione si annulla in $(a_0,b_0)$ e possiamo passare al passo successivo. Questo passo viene eseguito solo una volta, poiché se le ipotesi del teorema degli zeri sono verificate sull'intervallo chiuso e limitato $[a,b]$, allora lo sono anche su ogni intervallo $[a',b']\subseteq [a,b]$ per la restrizione di $f$ a tale sottointervallo.
\item[1] valutiamo la funzione in $c_0$
  \begin{itemize}
  \item se $f(c_0)=0$, abbiamo trovato \emph{uno} zero di $f$ e terminiamo l'algoritmo.
  \item se $f(c_0)>0$ e $f(a_0)<0<f(b_0)$ allora
    \[
      a_1\coloneqq a_0,\quad b_1\coloneqq c_0 
    \]
    altrimenti, se $f(c_0)>0$ e $f(b_0)<0<f(a_0)$ allora
    \[
      a_1\coloneqq c_0,\quad b_1\coloneqq b_0.
    \]
  \item se $f(c_0)<0$ e $f(a_0)<0<f(b_0)$ allora
    \[
      a_1\coloneqq c_0,\quad b_1\coloneqq b_0 
    \]
    altrimenti, se $f(c_0)<0$ e $f(b_0)<0<f(a_0)$ allora
    \[
      a_1\coloneqq a_0,\quad b_1\coloneqq c_0.
    \]
  \end{itemize}
Consideriamo la restrizione
  \[
    f|_{[a_1,b_1]}\colon [a_1,b_1]\subseteq \R\to\R,
    \]
    definiamo
    \[
    c_1\coloneqq\frac{a_1+b_1}{2}
    \]
    e ripetiamo con
    \[
c_0\rightsquigarrow c_1,\quad [a_0,b_0]\rightsquigarrow [a_1,b_1],\quad f|_{[a_0,b_0]}\rightsquigarrow f|_{[a_1,b_1]}.
    \]
\end{description}
A ogni passo successivo al primo (passo 0 o inizializzazione) l'algoritmo costruisce un intervallo
\[
  [a_n,b_n]\subseteq[a_{n-1},b_{n-1}]
\]
di lunghezza
\[
 l_n=\frac{b-a}{2^n}
\]
ed una funzione
\[
f|_{[a_n,b_n]}
\]
a cui possiamo applicare il teorema di Bolzano, e che possiede pertanto uno zero in $(a_n,b_n)$.

A meno che non si trovi uno zero della funzione, l'algoritmo non termina mai. Il punto medio dell'intervallo $c_n=\frac{a_n+b_n}{2}\in (a_n,b_n)$ può essere visto come un'\emph{approssimazione} dello zero della funzione, in quanto ad ogni iterazione dell'algoritmo la lunghezza dell'intervallo si dimezza e l'errore che si commette nell'approssimare lo zero della funzione con $c_n$ si riduce.

Se volessimo ad esempio ottenere un'approssimazione dello zero a meno di un errore di $1/10^N$ dovremmo iterare l'algoritmo $n$ volte, in modo da ottenere un intervallo di lunghezza $l/2^n$, dove $l\coloneqq b-a$. Pertanto è necessario che
\[
  \frac{l}{2^n}<\frac{1}{10^N},
\]
ovvero
\[
  n>(Nl)\cdot\frac{\ln10}{\ln2}.
\]
\begin{eg}
Partiamo con un esempio semplicissimo: cerchiamo gli zeri della funzione
\[
f\colon [0,2]\subseteq\R\to\R,\quad f\colon x\mapsto x^2-2.
\]
In questo caso siamo in grado di determinare l'unico zero della funzione nell'intervallo analiticamente, ed è 
\[
x=\sqrt2\approx1.41421356.
\]
Applichiamo l'algoritmo dicotomico per determinare lo zero in maniera approssimata: la funzione considerata è continua sull'intervallo chiuso e limitato $[0,2]$ e
\[
f(0)=-2<0<2=f(2),
\]
quindi esiste uno zero in $(0,2)$ per il teorema di Bolzano. Consideriamo ora (prima iterazione, $n=1$)
\[
c=\frac{0+2}{2}=1
\]
e valutiamo $f$ in $x=1$
\[
f(1)=-1<0,
\]
quindi consideriamo la restrizione di $f$ all'intervallo chiuso e limitato $[1,2]$ (di lunghezza $1$). La funzione
\[
f|_{[1,2]}\colon[1,2]\subseteq\R\to\R
\]
soddisfa ancora le ipotesi del teorema di Bolzano, pertanto ammette uno zero in $(1,2)$; consideriamo ($n=2$)
\[
c=\frac{1+2}{2}=\frac32,
\]
e valutiamo la funzione $f$ in $c=\frac32$
\[
f\Big(\frac32\Big)=\frac14>0,
\]
dunque, al passo successivo dell'algoritmo considereremo la restrizione di $f$
\[
f|_{\left[1,\frac32\right]}\colon\left[1,\frac32\right]\subseteq\R \to\R.
\]
Iterando ancora otteniamo
\begin{itemize}
\item[$n=3$] 
\[
c=\frac{3/2+1}{2}=\frac54,\quad f\Big(\frac54\Big)=-\frac{7}{16}<0\rightsquigarrow \left[\frac54,\frac32\right];
\]
\item[$n=4$] 
\[
c=\frac{5/4+3/2}{2}=\frac{11}{8},\quad f\Big(\frac{11}{8}\Big)<0\rightsquigarrow \left[\frac{11}{8},\frac32\right];
\]
\item[$n=5$]
\[
c=\frac{11/8+3/2}{2}=\frac{23}{16},\quad f\Big(\frac{23}{16}\Big)>0\rightsquigarrow \left[\frac{11}{8},\frac{23}{16}\right].
\]
\end{itemize}
Alla quinta iterazione la funzione cambia segno nel punto medio dell'intervallo considerato: abbiamo localizzato lo zero tra $\frac{11}{8}\approx 1.375$ e $\frac{23}{16}\approx1.4375$. Alla cinquantesima iterazione otteniamo
\[
n=50:\quad [1.414213562373094,1.4142135623730958],\quad x\approx1.414213562373095.
\]
\end{eg}
\begin{exc}
Dopo aver verificato che la funzione
\[
f\colon[0,1]\subseteq\R\to\R\,\quad f\colon x\mapsto \e^x+x-2
\]
soddisfa le ipotesi del teorema di Bolzano (o degli zeri), utilizzare l'algoritmo dicotomico (o metodo di bisezione) per localizzare lo zero con una precisione di $1/10$.
\end{exc}
\begin{proof}[Soluzione]
La funzione $f$ è continua in quanto somma di funzioni continue, inoltre
\[
f(0)=-1<0<\e-1=f(1).
\]
quindi le ipotesi del teorema sono soddisfatte. 
Per individuare in maniera approssimata l'intervallo in cui lo zero si trova, possiamo tracciare i grafici delle due funzioni
\[
	g(x)=\e^x,\quad h(x)=2-x
\]
e cercare il loro punto di intersezione. A quel punto verifichiamo che negli estremi dell'intervallo da noi scelto la funzione effettivamente cambi segno.
\begin{figure}[h!]
\centering
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
  \begin{scriptsize}
    \begin{axis}[
      x=1cm,y=1cm,
      axis lines=middle,
      xmin=-3,
      xmax=3.5,
      ymin=-.5,
      ymax=2.5,
      ticks=none,
      xlabel=$x$,ylabel=$y$
      ]
      \clip(-3,-.5) rectangle (3,2.5);
      \draw[draw=black, line width=.5pt, dash pattern = on 3pt off 3pt] (1,0) node[below]{$+1$}-- (1,5);
      \addplot[domain=-5:5,samples=501,smooth, line width=1pt, color=DarkSlateBlue] {e^(\x)};
      \addplot[domain=-5:5,samples=500,smooth, line width=1pt, color=SteelBlue] {-\x+2};
      \draw[fill=black,draw=black] (0,1) circle (1pt) node[left]{$+1$};
      \draw[fill=black,draw=black] (2,0) circle (1pt) node[below,xshift=-5pt]{$+2$};
      \draw[fill=black,draw=black] (0,2) circle (1pt) node[left,yshift=-5pt]{$+2$};
      \draw[draw=black] (0.442854, 1.55715) circle (5pt);
      \draw[draw=Crimson,fill=Crimson] (0.442854, 1.55715) circle (1pt);
      \draw[draw=SeaGreen,opacity=.25,line width=3pt] (0,0)--(1,0);
    \end{axis}
  \end{scriptsize}
\end{tikzpicture}\caption{Dai grafici delle funzioni $g$ e $h$ deduciamo che lo zero di $f$ si trova nell'intervallo $[0,1]$.}
\end{figure}

Per determinare lo zero approssimato con una precisione di $1/10$ servono
\[
n>\ln(10)/\ln(2)\approx3.32
\]
iterazioni dell'algoritmo, ovvero almeno $4$ iterazioni. Procediamo: alla prima iterazione
\[
n=1:\quad c=\frac12,\quad f\Big(\frac12\Big)>0
\]
quindi lo zero si trova in $\left(0,\frac12\right)$. Ripetiamo
\[
n=2:\quad c=\frac{1/2}{2}=\frac14,\quad f\Big(\frac14\Big)<0
\]
quindi lo zero si trova in $\left(\frac14,\frac12\right)$. Ripetiamo ancora
\[
n=3:\quad c=\frac{1/4+1/2}{2}=\frac38,\quad f\Big(\frac38\Big)<0
\]
quindi lo zero si trova in $\left(\frac38,\frac12\right)$. Infine, iterando un'ultima volta
\[
n=4:\quad c=\frac{3/8+1/2}{2}=\frac{7}{16},\quad f\Big(\frac{7}{16}\Big)<0
\]
quindi lo zero si trova in $\left(\frac{7}{16},\frac12\right)$, ovvero tra $0.4375$ e $0.5$. Per tracciare il grafico qualitativo di una funzione un valore con un'approssimazione del genere è più che sufficiente.
\end{proof}
\begin{figure}[b]\centering
  \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
    \begin{scriptsize}
\begin{axis}[
x=1cm,y=3cm,
axis lines=middle,
xmin=-3,
xmax=3.5,
ymin=-.5,
ymax=.75,
ticks=none,
xlabel=$x$,ylabel=$y$
]
\clip(-3,-.5) rectangle (3,.75);
\addplot[domain=-2:2,samples=501,smooth, line width=1pt, color=DarkSlateBlue] {((2*\x-1)*(2*\x+1))/( sqrt(9-(\x)^2) * e^((\x)^2) ) };
\addplot[domain=-2:2,samples=500,smooth, line width=.5pt, color=Crimson, dash pattern=on 3pt off 3pt] {0.412566981856047};
\draw (0,0.412566981856047) node[above,xshift=10pt]{$M$};
\draw (0,-0.333333333333333) node[below,xshift=10pt] {$m$};
\draw (-2,0) node[below]{$-2$};
\draw (2,0) node[below]{$2$};
\draw[draw=black, line width=.5pt, dash pattern = on 3pt off 3pt] (-1.14875338374606,0) node[below]{$x_1$}-- (-1.14875338374606,0.412566981856047);
\draw[draw=black, line width=.5pt, dash pattern = on 3pt off 3pt] (1.14875338374606,0) node[below]{$x_2$}-- (1.14875338374606,0.412566981856047);
\draw[fill=Crimson,draw=Crimson] (1.14875338374606,0.412566981856047) circle (1pt) node[below]{};
\draw[fill=Crimson,draw=Crimson] (-1.14875338374606,0.412566981856047) circle (1pt) node[below]{};
\draw[color=Crimson,fill=Crimson] (0,-0.333333333333333) circle (1pt);
\end{axis}
\end{scriptsize}
\end{tikzpicture}
\caption{Grafico della funzione $g$ dell'Esercizio~\ref{exc:ivt}: $M=8\sqrt{\frac{70}{941+33\sqrt{809}}}\e^{\frac{1}{8} (\sqrt{809}-39)},\,x_{12}=\pm\frac12\sqrt{\frac12(39-\sqrt{809})},\,m=-\frac13$.}
\label{fig:ivt}
\end{figure}
\begin{exc}[per casa]\label{exc:ivt}
Per quali valori del parametro $\lambda\in\R$ la funzione
\[
f\colon [-2,2]\subseteq\R\to\R,\quad f\colon x\mapsto g(x)-\lambda
\]
ammette sicuramente degli zeri? Il grafico della funzione 
\[
g\colon[-2,2]\subseteq\R\to\R,\quad g\colon x\mapsto \frac{4x^2-1}{\e^{x^2}\cdot\sqrt{9-x^2}}
\]
è rappresentato in Figura~\ref{fig:ivt}. Giustificare la risposta.
\end{exc}
\begin{exc}[per casa]
Dopo aver verificato che la funzione
\[
f\colon\left[\frac{1}{10},\frac{9}{10}\right]\subseteq\R\to\R,\quad f\colon x\mapsto \ln(x)-x+2
\]
soddisfa le ipotesi di teorema di Bolzano (o degli zeri), utilizzare l'algoritmo dicotomico (o metodo di bisezione) per localizzare lo zero con una precisione di $1/10$. Ripetere sull'intervallo $[3,4]$.
\end{exc}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
