(TeX-add-style-hook
 "1C"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "12pt" "final")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontspec" "no-math") ("babel" "italian") ("xcolor" "svgnames") ("hyperref" "colorlinks=true" "linkcolor=DarkSlateBlue")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir12"
    "inputenc"
    "fontspec"
    "babel"
    "microtype"
    "amsmath"
    "amssymb"
    "amsthm"
    "bm"
    "mathtools"
    "enumitem"
    "siunitx"
    "xcolor"
    "hyperref"
    "tikz"
    "tkz-euclide"
    "subcaption")
   (TeX-add-symbols
    '("pd" 1)
    "wideparen")
   (LaTeX-add-labels
    "item:a"
    "item:b")
   (LaTeX-add-amsthm-newtheorems
    "exc"
    "thm"
    "cor"
    "prp"
    "dfn")
   (LaTeX-add-amsthm-newtheoremstyles
    "rmk"
    "thm"
    "dfn")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("norm" "")))
 :latex)

