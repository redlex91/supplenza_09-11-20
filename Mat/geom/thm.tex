\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}


\DeclareFontFamily{OMX}{yhex}{}
\DeclareFontShape{OMX}{yhex}{m}{n}{<->yhcmex10}{}
\DeclareSymbolFont{yhlargesymbols}{OMX}{yhex}{m}{n}
\DeclareMathAccent{\wideparen}{\mathord}{yhlargesymbols}{"F3}

\usepackage{enumitem}
\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}

\usepackage{tikz,tkz-euclide,subcaption}
\usetikzlibrary{arrows}

\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{rmk}
\newtheorem{exc}{Esercizio}

\newtheoremstyle{thm}
{\topsep}
{\topsep}
{\itshape}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{thm}
\newtheorem{thm}{Teorema}
\theoremstyle{thm}
\newtheorem{cor}{Corollario}
\theoremstyle{thm}
\newtheorem{prp}{Proposizione}

\renewcommand{\qedsymbol}{\color{DarkSlateBlue}\bfseries\sffamily QED}

\newtheoremstyle{dfn}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{dfn}
\newtheorem{dfn}{Definizione}

\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{part}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\usepackage[cal=boondoxo]{mathalfa}

\title{Note sull'intersezione di figure convesse}
\author{Alessandro Rossi}

\begin{document}

\maketitle

I teoremi, a seconda della loro importanza e rilevanza all'interno di una teoria, si suddividono in:
\begin{itemize}
\item proposizioni: proposizione è il termine generico per teorema e sono proposizioni la maggior parte dei teoremi che costituiscono la teoria matematica;
\item lemmata (al singolare lemma): proposizioni di carattere tecnico che servono per dimostrare altre proposizioni;
\item corollari: proposizioni che seguono come conseguenze dirette di altre proposizioni;
\item teoremi propriamente detti: ovvero quelle proposizioni che sono particolarmente di rilievo nella teoria; spesso hanno un nome oppure sono attribuiti a qualche matematico famoso.
\end{itemize}
Un teorema si compone di due parti: l'\emph{enunciato}, che esprime il contenuto del teorema e la \emph{dimostrazione}, che contiene la sequenza di passaggi logici che prova la validità del teorema, basandosi su assiomi, postulati e proposizioni precedentemente dimostrate. L'enunciato è nella forma di \emph{implicazione logica}
\[
p\rightarrow q
\]
dove $p$ prende il nome di \emph{ipotesi} (o antecedente) e $q$ prende il nome di \emph{tesi} (o conseguenza). Quando si vuole formulare o studiare un teorema, il primo passaggio fondamentale consiste nell'individuare ipotesi e tesi. L'implicazione logica può essere letta nel linguaggio naturale in molti modi, ad esempio
\begin{itemize}
\item se $p$, allora $q$;
\item da $p$ segue $q$;
\item $q$ è conseguenza di $p$;
\item $p$ implica $q$;
\item $p$ è condizione sufficiente per $q$;
\item $q$ è condizione necessaria per $p$;
\item è sufficiente che $p$ affinché $q$;
\item è necessario che $q$ affinché $p$.
\end{itemize}
Consideriamo come esempio l'enunciato del teorema che andremo a dimostrare a breve
\begin{quote}
Se $\mathcal{F}$ e $\mathcal{G}$ sono due figure convesse, allora l'intersezione $\mathcal{F}\cap\mathcal{G}$ è essa stessa convessa.
\end{quote}
Le ipotesi sono:
\begin{itemize}
\item $\mathcal{F}$ è una figura convessa, e
\item $\mathcal{G}$ è una figura convessa.
\end{itemize}
La tesi è:
\begin{itemize}
\item l'intersezione delle due figure $\mathcal{F}\cap\mathcal{G}$ è convessa.
\end{itemize}
L'insieme vuoto e l'insieme costituito da un solo punto si assumono convessi per definizione; sono inoltre convessi: i segmenti, le rette, le semirette, i piani ed i semipiani. Poiché in geometria euclidea la figura che non è costituita da alcun punto non è particolarmente interessante, si può tranquillamente escludere l'insieme vuoto dall'elenco delle figure convesse, ma, negli sviluppi moderni della geometria, l'insieme vuoto viene considerato convesso per definizione.
\begin{prp}
Se $\mathcal{F}$ e $\mathcal{G}$ sono due figure convesse, allora la loro intersezione è convessa.
\end{prp}
\begin{figure}[h!]\centering
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
\clip(-5,-2) rectangle (5,1.5);
\fill[line width=2pt,color=black,fill=DarkSlateBlue,fill opacity=0.1] (-1.98,0) -- (-1.74,-1.33) -- (-0.5505265758714245,-1.9716018806797884) -- (0.6927225231808856,-1.441666339594253) -- (1.0535553679106573,-0.1392456478958215) -- (0.26025746658772486,0.954910849026182) -- (-1.0898016807636162,1.0168809922644428) -- cycle;
\fill[line width=2pt,color=black,fill=SeaGreen,fill opacity=0.1] (-0.82,-0.49) -- (-0.3,-1.39) -- (0.6768519591397535,-1.7451904417700792) -- (1.6534759892375441,-1.389373770103969) -- (2.172898852643539,-0.4890405601360611) -- (1.9920768188279685,0.5345332731006042) -- (1.1956185270261952,1.2024061572785736) -- (0.15619566362020065,1.2020729473106657) -- (-0.6398342617039828,0.5336895558440808) -- cycle;
\draw [line width=1pt,color=black] (-1.98,0)--(-1.74,-1.33);
\draw [line width=1pt,color=black]  (-0.3,-1.39)--(-0.82,-0.49);
\draw [line width=1pt,color=black] (-1.74,-1.33)-- (-0.5505265758714245,-1.9716018806797884);
\draw [line width=1pt,color=black] (-0.5505265758714245,-1.9716018806797884)-- (0.6927225231808856,-1.441666339594253);
\draw [line width=1pt,color=black] (0.6927225231808856,-1.441666339594253)-- (1.0535553679106573,-0.1392456478958215);
\draw [line width=1pt,color=black] (1.0535553679106573,-0.1392456478958215)-- (0.26025746658772486,0.954910849026182);
\draw [line width=1pt,color=black] (0.26025746658772486,0.954910849026182)-- (-1.0898016807636162,1.0168809922644428);
\draw [line width=1pt,color=black] (-1.0898016807636162,1.0168809922644428)-- (-1.98,0);
\draw [line width=1pt,color=black] (-0.3,-1.39)-- (0.6768519591397535,-1.7451904417700792);
\draw [line width=1pt,color=black] (0.6768519591397535,-1.7451904417700792)-- (1.6534759892375441,-1.389373770103969);
\draw [line width=1pt,color=black] (1.6534759892375441,-1.389373770103969)-- (2.172898852643539,-0.4890405601360611);
\draw [line width=1pt,color=black] (2.172898852643539,-0.4890405601360611)-- (1.9920768188279685,0.5345332731006042);
\draw [line width=1pt,color=black] (1.9920768188279685,0.5345332731006042)-- (1.1956185270261952,1.2024061572785736);
\draw [line width=1pt,color=black] (1.1956185270261952,1.2024061572785736)-- (0.15619566362020065,1.2020729473106657);
\draw [line width=1pt,color=black] (0.15619566362020065,1.2020729473106657)-- (-0.6398342617039828,0.5336895558440808);
\draw [line width=1pt,color=black] (-0.6398342617039828,0.5336895558440808)-- (-0.82,-0.49);
\draw [line width=1pt] (0.1,0.49)-- (-0.2,-1.03);
\begin{scriptsize}
\draw [fill=black] (0.1,0.49) circle (1pt)node[left] {$A$};
\draw [fill=black] (-0.2,-1.03) circle (1pt) node[right] {$B$};
\draw (-.48,-.42) node[xshift=-20pt] {$\mathcal{F}$};
\draw (.68,-.23) node[xshift=20pt] {$\mathcal{G}$};
\end{scriptsize}
\end{tikzpicture}
\end{figure}

\begin{proof}
Siano $A$ e $B$ due qualunque punti dell'intersezione $\mathcal{F}\cap\mathcal{G}$ delle due figure, questo significa che i punti $A$ e $B$ giacciono sia in $\mathcal{F}$ sia in $\mathcal{G}$, dunque
\begin{itemize}
\item il segmento $AB$ è contenuto in $\mathcal{F}$ poiché $\mathcal{F}$ è convessa: $AB\subseteq\mathcal{F}$, e
\item il segmento $AB$ è contenuto in $\mathcal{G}$ poiché $\mathcal{G}$ è convessa: $AB\subseteq\mathcal{G}$.
\end{itemize}
pertanto il segmento $AB$ è contenuto sia in $\mathcal{F}$ sia in $\mathcal{G}$
\[
AB\subseteq \mathcal{F}\wedge AB\subseteq\mathcal{G},
\]
pertanto il segmento $AB$ è contenuto nell'intersezione delle due figure: $AB\subseteq\mathcal{F}\cap\mathcal{G}$. Poiché i due punti $A$ e $B$ sono stati scelti in modo arbitrario, abbiamo provato che presi due qualunque punti $A,B\in\mathcal{F}\cap\mathcal{G}$ allora $AB\subseteq\mathcal{F}\cap\mathcal{G}$, ovvero l'intersezione delle due figure è convessa.
\end{proof}
La precedente proposizione ha delle interessanti conseguenze, perché ci permette di provare che una figura è convessa pensandola come intersezione di figure convesse, invece di ricorrere alla definizione.

\begin{dfn}
Presi tre punti non allineati $A,B,C$, un poligono avente per vertici i tre punti e per lati i segmenti che hanno per estremi i tre punti è detto \emph{triangolo}.
\end{dfn}
Notiamo che la definzione è \emph{ben posta} per via degli assiomi di appartenenza: infatti, data una retta, esiste sempre un punto del piano che non giace sulla retta, ovvero è possibile trovare tre punti non allineati (assioma 2.c pag. 5). Vogliamo dimostrare che i triangoli sono poligono convessi, utilizzando la proposizione appena dimostrata.
\begin{exc}
Dimostrare che i triangoli sono poligoni convessi.
\end{exc}
\begin{figure}[h!]\centering
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
\clip(-3,-2) rectangle (2.5,2);
\fill[line width=2pt,color=White,fill=white,fill opacity=1] (-2,0) -- (0,-1) -- (1,1) -- cycle;
\fill[line width=2pt,color=SteelBlue,fill=SteelBlue,fill opacity=0.25] (7.36,3.12) -- (-6.692,-1.564) -- (-11.791180031838675,-15.470722223547059) -- (-4.097753527224884,-28.128121190874666) -- (10.59497243856655,-30.004917315699664) -- (21.2230755363047,-19.68784483636481) -- (19.783377359599175,-4.94586975973445) -- cycle;
\fill[line width=2pt,color=SeaGreen,fill=SeaGreen,fill opacity=0.25] (-1.724,-4.448) -- (2.412,3.824) -- (-1.4765562024878207,12.215162652463215) -- (-10.461506474899181,14.406771331560604) -- (-17.777000002524026,8.748500001262014) -- (-17.914320950187992,-0.4988576190170164) -- (-10.770063843463266,-6.371852629487671) -- cycle;
\fill[line width=2pt,color=Crimson,fill=Crimson,fill opacity=0.25] (-6.294277166860185,2.1471385834300927) -- (7.611876784372643,-4.805938392186321) -- (18.521883847659698,6.2709809910746195) -- (11.358485079039697,20.06997063618882) -- (-3.9787458982236608,17.521295864016306) -- cycle;

\draw [line width=1pt,draw=SteelBlue,domain=-5:5] plot(\x,{(--2--1*\x)/3});
\draw [line width=1pt,draw=Crimson,domain=-15.32:15.32] plot(\x,{(-2-1*\x)/2});
\draw [line width=1pt,draw=SeaGreen,domain=-15.32:15.32] plot(\x,{(-1--2*\x)/1});
\draw [line width=1pt,draw=Crimson] (-2,0)-- (0,-1) node[near start,sloped,above,xshift=-1cm]{$c$};
\draw [line width=1pt,draw=SeaGreen] (0,-1)-- (1,1) node[sloped,near end, above,xshift=1cm]{$a$};
\draw [line width=1pt,draw=SteelBlue] (1,1)-- (-2,0) node[near end,sloped,below,xshift=-1.5cm]{$b$};
\begin{scriptsize}
\draw [fill=black] (-2,0) circle (1pt) node[below] {$A$};
\draw [fill=black] (0,-1) circle (1pt) node[below,xshift=1pt] {$B$};
\draw [fill=black] (1,1) circle (1pt) node[below,xshift=3pt] {$C$};
\end{scriptsize}
\end{tikzpicture}
\end{figure}
\begin{proof}
La strategia dimostrativa è la seguente: scriviamo il triangolo $\triangle ABC$ come intersezione di semipiani, che sappiamo essere figure convesse. Siano
\begin{itemize}
\item $a$ la retta per $B$ e $C$ e $\alpha$ il semipiano di origine $a$ che contiene $A$;
\item $b$ la retta per $A$ e $C$ e $\beta$ il semipiano di origine $b$ che contiene $B$;
\item $c$ la retta per $B$ e $A$ e $\gamma$ il semipiano di origine $c$ che contiene $C$.
\end{itemize}
Il triangolo è intersezione dei semipiani, in particolare i suoi lati giacciono sulle origini dei semipiani
\[
	\triangle ABC=(\alpha\cap\beta)\cap\gamma,
\]
pertanto è convesso essendo $\alpha,\beta,\gamma$ figure convesse.
\end{proof}
\begin{cor}
Gli angoli interni di un triangolo sono angoli convessi.
\end{cor}
\begin{proof}
La dimostrazione segue immediatamente dalla proposizione precedente poiché gli angoli interni di un triangolo sono intersezione di due semipiani.
\end{proof}
\begin{exc}[per casa]
Due rette $l$ e $l'$ sono \emph{parallele} se coincidono oppure non hanno punti in comune (pag. 102). La \emph{striscia compresa fra due rette} parallele e distinte $l$ e $l'$ è la figura costituita dai punti che appartengono sia al semipiano di origine $l$ che contiene $l'$, sia al semipiano di origine $l'$ che contiene $l$.  Formalizzare il fatto che una striscia è una figura convessa: scrivere l'enunciato della proposizione, individuare ipotesi e tesi, elencare i passaggi logici necessari a dimostrare che una striscia è una figura convessa ed infine scrivere la dimostrazione in maniera dettagliata imitando quanto fatto per i triangoli.
\end{exc}
\begin{exc}[per casa]
Un \emph{trapezio} è un quadrilatero con due soli lati paralleli (pag. 141). Dimostrare che i trapezi sono quadrilateri convessi, descrivendoli come intersezione fra una striscia e un angolo convesso oppure un triangolo opportuno. Scrivere l'enunciato della proposizione, individuare ipotesi e tesi, elencare i passaggi logici necessari a dimostrare che una striscia è una figura convessa ed infine scrivere la dimostrazione in maniera dettagliata imitando quanto fatto per i triangoli.
\end{exc}
\begin{exc}[per casa]
Un \emph{parallelogramma} è un quadrilatero coi lati non consecutivi paralleli (pag. 143). Dimostrare che un quadrilatero è un poligono convesso, descrivendolo come intersezione di due strisce opportune. Scrivere l'enunciato della proposizione, individuare ipotesi e tesi, elencare i passaggi logici necessari a dimostrare che una striscia è una figura convessa ed infine scrivere la dimostrazione in maniera dettagliata imitando quanto fatto per i triangoli.
\end{exc}
\end{document}
