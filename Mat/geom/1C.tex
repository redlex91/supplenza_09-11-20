\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}


\DeclareFontFamily{OMX}{yhex}{}
\DeclareFontShape{OMX}{yhex}{m}{n}{<->yhcmex10}{}
\DeclareSymbolFont{yhlargesymbols}{OMX}{yhex}{m}{n}
\DeclareMathAccent{\wideparen}{\mathord}{yhlargesymbols}{"F3}


\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother

\usepackage{enumitem}
\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}

\usepackage{tikz,tkz-euclide,subcaption}
\usetikzlibrary{arrows}

\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{rmk}
\newtheorem{exc}{Esercizio}

\newtheoremstyle{thm}
{\topsep}
{\topsep}
{\itshape}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{thm}
\newtheorem{thm}{Teorema}[chapter]
\theoremstyle{thm}
\newtheorem{cor}{Corollario}[chapter]
\theoremstyle{thm}
\newtheorem{prp}{Proposizione}[chapter]


\newtheoremstyle{dfn}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{dfn}
\newtheorem{dfn}{Definizione}[chapter]

\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{part}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\title{Esercizi di Geometria per la 1CL}
\author{Alessandro Rossi}

\begin{document}

\maketitle

\begin{exc}
  Leggere la seguente definizione
  \begin{quote}
    Date due rette $l$ e $m$ nel piano, diciamo che queste sono \emph{parallele}, in simboli $l\parallel m$, se
    \begin{itemize}
    \item coincidono ($l=m$), oppure
    \item non hanno punti in comune ($l\cap m=\emptyset$).
    \end{itemize}
  \end{quote}
  e dire quale o quali delle seguenti figure rappresentano due rette parallele.
  \begin{figure}[!h]\centering
    \begin{subfigure}[t]{.3\textwidth}
      \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
        \clip(-1.5,-1.5) rectangle (1.5,1.5);
        \draw [line width=1pt,draw=DarkSlateBlue,domain=-11.3:19.34] plot(\x,{(-0--1*\x)/1});
        \begin{scriptsize}
          \draw [fill=black] (0,0) node[label=0:$l{=} m$]{};
        \end{scriptsize}
      \end{tikzpicture}\caption{}
    \end{subfigure}
    \begin{subfigure}[t]{.3\textwidth}
      \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]

        \clip(-1.5,-1.5) rectangle (1.5,1.5);
        \draw [line width=1pt,draw=DarkSlateBlue,domain=-11.68:18.96] plot(\x,{(--1--1*\x)/1});
        \draw [line width=1pt,draw=Crimson] (0,-8.74) -- (0,9.64);
        \begin{scriptsize}
          \draw [fill=black] (-1,0)  node[label=180:$l$]{};
          \draw [fill=black] (0,-1) node[label=180:$m$]{};
          \draw [fill=black] (0,1) circle (1pt) node[label=0:$P$]{};
        \end{scriptsize}
      \end{tikzpicture}\caption{}
    \end{subfigure}
    \begin{subfigure}[t]{.3\textwidth}
      \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
        \clip(-1.5,-1.5) rectangle (1.5,1.5);
        \draw [line width=1pt,domain=-14.3:16.34,draw=DarkSlateBlue] plot(\x,{(--0.82--1.07*\x)/0.82});
        \draw [line width=1pt,domain=-14.3:16.34,draw=Crimson] plot(\x,{(-1.07--1.07*\x)/0.82});
        \begin{scriptsize}
          \draw [fill=black] (-0.82,-0.07) node[label=180:$l$]{};
          \draw [fill=black] (1,0) node[label=180:$m$]{};
        \end{scriptsize}
      \end{tikzpicture}\caption{}
    \end{subfigure}
  \end{figure}

  Dire se la seguente affermazione è vera o falsa, motivando la risposta citando uno degli assiomi della Geometria Euclidea studiati finora.
  \begin{quote}
    Due rette $l$ e $m$ che hanno due punti distinti in comune non sono parallele ($l\cap m=\lbrace P,Q\rbrace,\, P\ne Q$).
  \end{quote}
\end{exc}
\begin{exc}
  L'\emph{assioma di partizione del piano da parte di una retta} stabilisce che: scelta una qualunque retta $l$ nel piano, il complementare di $l$ è suddiviso in due sottoinsiemi $\alpha$ e $\beta$ disgiunti e convessi tali che
  \begin{quote}
    per ogni coppia di punti $A\in\alpha$ e $B\in\beta$ il segmento $AB$ interseca $l$ in \emph{uno ed un solo punto}.\hfill(\ast)
  \end{quote}
  Dire se le seguenti affermazioni sono vere o false.
  \begin{enumerate}
  \item $l$ non appartiene né ad $\alpha$ né a $\beta$.\label{item:c}
  \item è necessario che la condizione evidenziata con (\ast) alla fine della linea sia soddisfatta per \emph{qualunque} $A\in\alpha$ e $B\in\beta$.
  \item è sufficiente che la condizione evidenziata con (\ast) alla fine della linea sia soddisfatta per \emph{almeno} una coppia di punti $A\in\alpha$ e $B\in\beta$.
  \item il segmento $AB$ interseca $l$ in \emph{almeno} un punto.
  \item il segmento $AB$ interseca $l$ in \emph{al più} un punto.
  \item il segmento $AB$ potrebbe non intersecare $l$ in alcun punto.
  \item il segmento $AB$ potrebbe intersecare la retta $l$ in due punti distinti.
  \item i punti $A$ e $B$ potrebbero coincidere.\label{item:a}
  \item il segmento $AB$ potrebbe giacere lungo la retta $l$ ($AB\subseteq l$).\label{item:b}
  \end{enumerate}
  Giustificare con particolare attenzione i punti \ref{item:c}, \ref{item:a} e \ref{item:b} e fare un disegno che illustri la situazione descritta dall'assioma.
\end{exc}

\begin{exc}
  Si considerino due rette $l$ e $m$ parallele e non coincidenti. Ciascuna retta individua due semipiani, pertanto le due rette individuano in tutto quattro semipiani.
  \begin{enumerate}
  \item Disegnare ciascun semipiano (ognuno in una figura diversa) e associare a ciascuno la caratterizzazione appropriata:
    \begin{enumerate}
    \item $\alpha$ è il semipiano di origine $l$ che non contiene $m$;
    \item $\beta$ è il semipiano di origine $l$ che contiene $m$;
    \item $\alpha'$ è il semipiano di origine $m$ che non contiene $l$;
    \item $\beta'$ è il semipiano di origine $m$ che contiene $l$.
    \end{enumerate}
    \begin{proof}[Esempio di svolgimento] $\alpha$ è il semipiano di origine $l$ che non contiene $m$.
    \begin{center}
      \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
        \clip(-1.5,-1.5) rectangle (1.5,1.5);
        \fill[line width=1pt,color=blue,fill=DarkSlateBlue,fill opacity=0.1] (-2.002318907660264,-3.466711409395973) -- (1.8564637815320526,5.141342281879194) -- (-1.5017779497333592,13.956726826450904) -- (-10.109831641008524,17.81550951564322) -- (-18.925216185580233,14.457267784377809) -- (-22.783998874772553,5.849214093102644) -- (-19.42575714350714,-2.9661704514690674) -- (-10.817703452231978,-6.824953140661386) -- cycle;
        \draw [line width=1pt,draw=DarkSlateBlue,domain=-8.98:21.66] plot(\x,{(--0.52--1.16*\x)/0.52});
        \draw [line width=1pt,draw=Crimson,domain=-8.98:21.66] plot(\x,{(-0.8312--1.16*\x)/0.52});
        \begin{scriptsize}
          \draw [fill=black] (-0.52,-0.16) node[label=0:$l$]{};
          \draw [fill=black] (0,1) node[xshift=-5pt,label=:$\alpha$]{};
          \draw [fill=black] (0.34,-0.84) node[label=0:$m$]{};
        \end{scriptsize}
      \end{tikzpicture}
    \end{center}
    \end{proof}
  \item  Per ciascuna coppia di semipiani (l'ordine è ininfluente)
  \[
	\alpha,\alpha';\qquad\alpha,\beta';\qquad\beta,\alpha';\qquad\beta,\beta'
  \]
   disegnare unione ed intersezione e dire se queste sono
    \begin{itemize}
    \item l'insieme vuoto;
    \item l'intero piano;
    \item un semipiano;
    \item l'unione di due semipiani disgiunti;
    \item altro (solo un caso).
    \end{itemize}
    \begin{proof}[Esempio di svolgimento]$\alpha'\cup\beta=\beta$ è un semipiano, poiché $\alpha'\subseteq\beta$.
      \begin{center}
      \begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]
        \clip(-1.5,-1.5) rectangle (2,1.5);
        \fill[line width=1pt,color=black,fill=SeaGreen,fill opacity=0.1] (1.8564637815320526,5.141342281879194) -- (-2.002318907660264,-3.466711409395973) -- (6.605734783614901,-7.32549409858829) -- (10.46451747280722,1.2825595926868751) -- cycle;
        \draw [line width=1pt,draw=SeaGreen,domain=-8.98:21.66] plot(\x,{(--0.52--1.16*\x)/0.52});
        \draw [line width=1pt,domain=-8.98:21.66,draw=Crimson] plot(\x,{(-0.8312--1.16*\x)/0.52});
        \begin{scriptsize}
          \draw [fill=black] (-0.52,-0.16) node[label=0:$l$]{};
          \draw [fill=black] (0,1) node[label=0:$\alpha'\cup\beta{=}\beta$,xshift=3pt,yshift=-65pt]{};
          \draw [fill=black] (0.34,-0.84) node [label=0:$m$]{};
        \end{scriptsize}
      \end{tikzpicture}
    \end{center}
    \end{proof}
  \item Completare a parole gli spazi vuoti ${}^1,{}^2,{}^3$ nella seguente definizione in modo che descriva la situazione indicata con ``altro'' in termini di \textbf{due} dei semipiani introdotti sopra ($\alpha,\beta,\alpha',\beta'$). Quali proprietà devono soddisfare le due rette (${}^1,{}^2)$? Quale condizione devono verificare i punti della regione di piano indicata con ``altro'' (${}^3$)?
    \begin{quote}
      Siano $l$ e $m$ due rette ${}^1\phantom{distinte}$ e ${}^2\phantom{non coincidenti}$, chiamiamo \emph{striscia delimitata dalle rette} $l$ e $m$ il sottoinsieme di tutti e soli i punti del piano che ${}^3\phantom{giacciono nell'intersezione di a e b}$.
    \end{quote}
  \end{enumerate}
\end{exc}
\end{document}
