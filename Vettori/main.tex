\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}

\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother


\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathreplacing,angles,quotes}

\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}

\theoremstyle{exc}
\newtheorem{exc}{Esercizio}[chapter]
\newtheoremstyle{exc}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}

\theoremstyle{rmk}
\newtheorem{app}{Approfondimento}[chapter]
\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{Crimson}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{exc}
\newtheorem{eg}{Esempio}[chapter]
\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{section}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\title{Operazioni elementari sui vettori}
\author{Alessandro Rossi}

\begin{document}
\maketitle
\tableofcontents
\chapter{Vettori}
\section{Richiami}
Un \emph{vettore} è una grandezza geometrica $\vec{v}$ definita da tre parametri
\begin{itemize}
\item \emph{modulo} o \emph{lunghezza} del vettore (è un numero reale);
\item \emph{direzione}: la retta su cui giace il vettore;
\item \emph{verso}: preso un punto $P$ sulla retta su cui giace il vettore, il verso è la scelta di una delle due semirette in cui il punto $P$ divide la retta.
\end{itemize}
Due vettori $\vec{v}$ e $\vec{w}$ si sommano utilizzando la \emph{regola del parallelogramma} (vedi libro). Dato un qualunque vettore $\vec{v}$ possiamo definire il vettore \emph{opposto} di $\vec{v}$ che si indica con $-\vec{v}$, definito da
\begin{itemize}
\item ha lo stesso modulo $v=\norm{\vec{v}}$ di $\vec{v}$;
\item ha la stessa direzione di $\vec{v}$;
\item ha verso opposto a quello di $\vec{v}$; ciò significa che, se trasportiamo parallelamente a se stesso $-\vec{v}$ in modo che i due primi estremi dei due vettori coincidano, $\vec{v}$ e $-\vec{v}$ giacciono in due semirette opposte della retta di direzione di $\vec{v}$ rispetto all'estremo comune.
\end{itemize}
\section{Componenti di un vettore}
La descrizione astratta di vettore non è molto comoda per manipolare e fare conti coi vettori: per questo motivo introduciamo il concetto di \emph{componente}.
\begin{figure}[b]
\centering\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm,>=latex]
\clip(-4.5,-2) rectangle (8,5);
\begin{scriptsize}
\draw [color=gray!30, xstep=1cm,ystep=1cm] (-11.32,-9.59) grid (11.32,9.59);
\draw [shift={(0,0)},line width=.5pt,color=black,fill=black,opacity=0.10000000149011612] (0,0) -- (0:1.6) arc (0:33.690067525979785:1.6) -- cycle;
\draw [->,line width=.5pt] (0,-4) -- (0,4);
\draw [->,line width=.5pt] (-5,0) -- (7,0);


\draw [->,line width=1pt,draw=DarkSlateBlue] (-4,1) -- node[sloped,above,midway] {$\vec{v}$}(-1,3);
\draw [->,line width=1pt,draw=DarkSlateBlue] (0,0) -- node[sloped,above,midway] {$\vec{v}$}(3,2);
\draw [->,line width=1pt] (0,0) -- (1,0);
\draw [->,line width=1pt] (0,0) -- (0,1);
\draw[->,line width=1pt] (-4,1)--node[below,midway]{$v\cos\theta\vec{e}_1$} (-1,1);
\draw[->,line width=1pt] (-1,1)--node[below,midway,sloped]{$v\sin\theta\vec{e}_2$} (-1,3);

\draw[->,line width=1pt] (-4,3)--node[sloped,midway,below]{$\vec{e}_2$} (-4,4);
\draw[->,line width=1pt] (-4,3)--node[sloped,midway,below]{$\vec{e}_1$} (-3,3);


\draw [line width=.5pt,dashed] (0,2)-- (3,2);
\draw [line width=.5pt,dashed] (3,2)-- (3,0);
\draw [shift={(0,0)},->,line width=.5pt,color=black] (0:1.6) arc (0:33.690067525979785:1.6) node[midway,right] {$\theta$};

%\draw [fill=black] (7,0) circle (1pt);
%\draw[color=black] (7.16,0.44) node {$O$};

\draw[decoration={brace,mirror,raise=5pt},decorate]
  (3,0) -- node[below=6pt,sloped] {$v\sin\theta$} (3,2);
\draw[decoration={brace,mirror,raise=5pt},decorate]
  (0,0) -- node[below=6pt] {$v\cos\theta$} (3,0);
\draw [fill=black] (0,0) circle (1pt) node[label=225:$O$]{};
\draw [fill=black] (3,2) circle (1pt) node[label=45:$P$]{};
\draw [fill=black] (0,2) circle (1pt) node[label=180:$G$]{};
\draw [fill=black] (3,0) circle (1pt)node [label=270:$F$,xshift=2pt]{};
\draw [fill=black] (0,4) node[label=180:$y$]{};
\draw [fill=black] (7,0) node[label=270:$x$]{};

\end{scriptsize}
\end{tikzpicture}
\caption{Decomposizione di un vettore in componenti rispetto ad un sistema di vettori ortonormali}
\end{figure}
Supponiamo ci venga assegnato un sistema di riferimento ortogonale cartesiano $xOy$ ed un vettore $\vec{v}$; il vettore può trovarsi in un qualunque posto del piano, ma poiché il primo estremo del vettore non è fissato, possiamo sempre pensare di trasportarlo parallelamente a se stesso fino ad avere come primo estremo $O$: d'ora in avanti supporremo sempre che il vettore dato si trovi in questa posizione.

Introduciamo due vettori
\[
\begin{array}{lccc}
&\text{modulo}&\text{direzione}&\text{verso}\\
\vec{e}_1 & 1 & \text{direzione dell'asse }x& \text{verso positivo dell'asse }x\\
\vec{e}_2 & 1 & \text{direzione dell'asse }y &\text{verso positivo dell'asse }y
\end{array}
\]
diciamo che i due vettori $\lbrace \vec{e}_1,\vec{e}_2\rbrace$ formano \emph{un sistema di vettori ortonormali} (ovvero di modulo $1$ e ortogonali) oppure che sono una coppia di \emph{versori}. Qual è l'informazione contenuta nei due vettori $\vec{e}_1,\vec{e}_2$? I due vettori indicano semplicemente la direzione ed il verso degli assi cartesiani che abbiamo scelto o che ci hanno assegnato.\footnote{La lettera ``e'' sta per euclideo.}

Ora procediamo a decomporre il vettore: il suo primo estremo, come abbiamo già detto è $O$, mentre chiamiamo il suo secondo estremo $P$: da $P$ mandiamo le rette perpendicolari agli assi cartesiani che li intersecano in due punti $F$ e $G$ rispettivamente. Determinare le componenti del vettore $\vec{v}$ rispetto agli assi cartesiani $x,y$, o equivalentemente al sistema ortonormale associato $\lbrace \vec{e}_1,\vec{e}_2\rbrace$, significa determinare le lunghezze dei segmenti $\overline{OF},\overline{OG}$. Chiamiamo $\theta$ l'angolo formato dall'asse $x$ col vettore $\vec{v}$ in senso antiorario: utlizzando le funzioni trigonometriche, possiamo scrivere le lunghezze dei cateti del triangolo rettangolo $\triangle OFP$ in funzione della lunghezza dell'ipotenusa (il modulo $v$ del vettore) e dell'angolo $\theta$ (che in due dimensioni definisce la direzione ed il verso del vettore $\vec{v}$)
\[
\begin{split}
\cos\theta&=\frac{OF}{v}\implies OF=v\cos\theta,\\
\sin\theta&=\frac{OG}{v}\implies OG=v\sin\theta.
\end{split}
\]
Ora consideriamo i due vettori
\[
OF\vec{e}_1,\quad OG\vec{e}_2
\]
possiamo sommarli utilizzando la regola del parallelogramma: per costruzione $OF,OG$ sono lati consecutivi di un rettangolo, che è in particolare un parallelogramma, quindi la somma
\[
OF\vec{e}_1+OG\vec{e}_2
\] 
è la diagonale di questo rettangolo, che coicide esattamente col vettore $\vec{v}$; pertanto
\[
\vec{v}=OF\vec{e}_1+OG\vec{e}_2=v\cos\theta\vec{e}_1+v\sin\theta\vec{e}_2.
\]
Il numero $v\cos\theta$ è detto \emph{componente x} di $\vec{v}$, mentre il numero $v\sin\theta$ è detta \emph{componente y} di $\vec{v}$. Scrivendo il vettore in questo modo, possiamo facilmente fare conti coi vettori, senza preoccuparci troppo della geometria sottostante. Mentre il modulo di un vettore è \textbf{sempre} positivo ed è nullo solo per il vettore nullo, le componenti di un vettore possono avere sia segno positivo, sia segno negativo.

Vediamo un esempio concreto: ci viene dato un vettore $\vec{v}$ definito da
\begin{itemize}
\item modulo $v$ del vettore: $v=\SI{7}{N}$,
\item angolo $\theta$ formato da $\vec{v}$ con l'asse $x$ in senso antiorario: $\theta=\ang{60}$.
\end{itemize}
Vogliamo determinare le componenti del vettore rispetto al sistema ortonormale associato al sistema di riferimento ortogonale cartesiano. Procediamo come prima: come si può notare, la definizione di $\vec{v}$ non fa riferimento ad alcun punto particolare del piano, quindi possiamo assumere che il primo estremo di $\vec{v}$ sia $O$, l'origine del sistema di riferimento cartesiano; mandiamo le normali agli assi cartesiani con piedi $F$ (sull'asse $x$) e $G$ (sull'asse $y$) e calcoliamo la lunghezza dei segmenti $OF,OG$ utilizzando le funzioni goniometriche
\[
OF=v\cos\theta=\SI{7}{N}\cos\ang{60}=\frac{7}{2}\si{N},\quad OG=v\sin\theta=\SI{7}{N}\sin\ang{60}=\frac{7\sqrt{3}}{2}\si{N}.
\]
Ora che conosciamo le componenti del vettore ripetto agli assi cartesiani, possiamo scrivere
\[
\vec{v}=\underbracket[.5pt]{\frac{7}{2}\si{N}}_{\text{componente }x}\vec{e}_1+\underbracket[.5pt]{\frac{7\sqrt{3}}{2}\si{N}}_{\text{componente }y}\vec{e}_2.
\]
Attenzione! Le componenti di un vettore \textbf{dipendono} dalla scelta di un sistema di vettori ortonormali.
\section{Somma di vettori}

Le componenti di un vettore forniscono un dizionario per tradurre le caratteristiche geometriche del vettore astratto (che richiedono l'applicazione di teoremi della Geometria Euclidea e la determinazione di lunghezze mediante l'uso delle funzioni trigonometriche) in semplici calcoli di aritmetica (somme, prodotti). Saper dominare l'aritmetica delle componenti dei vettori è fondamentale per eseguire qualunque tipo di operazione sui vettori in modo efficiente.

Proviamo a prendere due vettori, già scritti in componenti
\[
\vec{v}=3\vec{e}_1-\vec{e}_2,\quad \vec{w}=4\vec{e}_1+5\vec{e}_2,
\]
vogliamo determinare la loro somma. Se rinunciamo ad utilizzare le componenti, dobbiamo anzitutto disegnare i due vettori e quindi applicare la regola del parallelogramma per determinare il vettore somma 
\[
\vec{a}=\vec{v}+\vec{w}.
\]
Per il momento, invece, limitiamoci a riscrivere il vettore somma $\vec{a}$, sostituendo al posto di $\vec{v}$ e $\vec{w}$ la loro decomposizione in componenti
\[
\vec{a}=(3\vec{e}_1-\vec{e}_2)+(4\vec{e}_1+5\vec{e}_2);
\]
in questo modo abbiamo semplicemente riscritto il vettore somma $\vec{a}$ utilizzando vettori tutti diretti lungo gli assi cartesiani: il vantaggio di questa operazione è che è molto più facile sommare vettori che hanno la medesima direzione (ovvero che sono paralleli) rispetto a sommare vettori qualunque; infatti è un'operazione banale, poiché
\[
3\vec{e}_1+4\vec{e}_1=(3+4)\vec{e}_1=7\vec{e}_1,
\]
\[
-\vec{e}_2+5\vec{e}_2=4\vec{e}_2.
\]
In questo modo possiamo riscrivere 
\[
\vec{a}=7\vec{e}_1+4\vec{e}_2,
\]
il vettore $\vec{a}$ risulta in questo modo determinato con pochi semplici e banali calcoli. L'idea è quindi quella di esprimere tutti i vettori in gioco in componenti rispetto ad una particolare scelta di sistema ortonormale e quindi svolgere poche basilari operazioni algebriche per eseguire l'operazione richiesta (ad esempio la somma di due vettori).

\begin{figure}[!ht]\centering

\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm,>=latex]

\clip(-1,-1) rectangle (10.5,5.5);
\draw [color=gray!30, xstep=1cm,ystep=1cm] (-30,-30) grid (30,9.30);

\begin{scriptsize}
\draw [->,line width=.5pt] (-8,0) -- (10,0)node[below]{$x$};
\draw [->,line width=.5pt] (0,-8) -- (0,5)node[left]{$y$};
\draw [shift={(0,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:1) arc (0:60:1) -- cycle;
\draw [shift={(6,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:1) arc (0:60:1) -- cycle;
\draw [shift={(0,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:1.5) arc (0:26.99550840111692:1.5) -- cycle;
\draw [line width=.5pt] (0,0)-- (5,0);
\draw [line width=.5pt] (0,0)-- (6,0);
\draw [->,line width=1pt,draw=Crimson] (0,0) -- node[sloped,above]{$\vec{b}$}(2.5,4.330127018922193);
\draw [->,line width=1pt,draw=SeaGreen] (0,0) --node[sloped,above]{$\vec{a}$} (6,0);
\draw [line width=.5pt,dashed] (0,4.330127018922193)-- (8.5,4.330127018922193);
\draw [line width=.5pt,dashed] (8.5,4.330127018922193)-- (8.5,0);
\draw [line width=1pt,->,draw=Crimson,draw opacity=.5] (6,0)--node[sloped,above]{$\vec{b}$} (8.5,4.330127018922193);
\draw [->,line width=1pt,draw=SeaGreen,draw opacity=.6] (02.5,4.33) --node[sloped,above]{$\vec{a}$} (8.5,4.33);
\draw [->,line width=1pt,draw=DarkSlateBlue] (0,0) -- node[sloped,midway,above]{$\vec{c}$}(8.5,4.330127018922193);
\draw [shift={(0,0)},->,line width=.5pt,color=black] (0:1) arc (0:60:1)node[midway,right,yshift=5pt]{$\theta$};
\draw [shift={(6,0)},->,line width=.5pt,color=black] (0:1) arc (0:60:1)node[midway,right]{$\theta$};
\draw [shift={(0,0)},->,line width=.5pt,color=black] (0:1.5) arc (0:26.99550840111692:1.5)node[midway,right]{$\gamma$};
\draw[->,line width=1pt] (9,4)--node[below,midway]{$\vec{e}_1$} (10,4);
\draw[->,line width=1pt] (9,4)--node[below,midway,sloped]{$\vec{e}_2$} (9,5);

\draw [line width=.5pt,dashed] (6,0)-- (8.5,4.330127018922193);

\draw [fill=black] (0,0) circle (1pt)node[label=225:$O$]{};

\draw [fill=black] (6,0) circle (1pt) node[label=-90:$P$]{};

\draw [fill=black] (2.5,4.330127018922193) circle (1pt) node[label=90:$R$]{};

\draw [fill=black] (8.5,4.330127018922193) circle (1pt) node[label=90:$Q$]{};

\draw [fill=black] (8.5,0) circle (1pt) node[label=-90:$F$]{};
\draw[fill=black] (0,4.33) circle (1pt) node[label=180:$G$]{};

\draw[decoration={brace,mirror,raise=3pt},decorate]
  (8.5,0) -- node[below=5pt,sloped] {$b\sin\theta$} (8.5,4.33);
  \draw[decoration={brace,mirror,raise=3pt},decorate]
  (0,0) -- node[below=5pt,sloped] {$a$} (6,0);
\draw[decoration={brace,mirror,raise=3pt},decorate]
  (6,0) -- node[below=5pt,sloped] {$b\cos\theta$} (8.5,0);
  \draw[decoration={brace,mirror,raise=3pt},decorate]
  (0,-.4) -- node[below=6pt,sloped] {$a+b\cos\theta$} (8.5,-.4);
\end{scriptsize}

\end{tikzpicture}
\caption{Somma di due vettori col metodo geometrico.}
\end{figure}

\paragraph{Somma con metodo geometrico}Proviamo a confrontare questo metodo, con quello che dovremmo seguire se non si volessero usare le componenti del vettore. Consideriamo due vettori
\[
\begin{split}
	\vec{a}:&\quad a=\SI{12}{m},\quad\text{diretto come l'asse }x,\quad\text{verso positivo dell'asse }x,\\
	\vec{b}:&\quad b=\SI{10}{m},\quad\text{forma un angolo }\theta=\ang{60}\text{ con l'asse }x,
	\end{split}
\]
vogliamo determinare la loro somma
\[
\vec{c}=\vec{a}+\vec{b}.
\]
Disegniamo i due vettori e determiniamo $\vec{c}$ mediante la regola del parallelogramma: il quadrilatero $OPQR$ è un parallelogramma, quindi i suoi lati opposti sono a due a due paralleli e
\[
F\widehat{O}R\cong F\widehat{P}Q\cong\theta
\] 
poiché angoli corrispondenti formati dalle rette parallele passanti per $O,R$ e $P,Q$ con la trasversale $x$. Dal secondo estremo $Q$ di $\vec{c}$ mandiamo le normali agli assi $x$ e $y$, le sue proiezioni su questi assi si scrivono come
\[
\begin{split}
&\text{proiezione lungo }x:\quad a+b\cos\theta,\\
&\text{proiezione lungo }y:\quad b\sin\theta,
\end{split}
\]
quindi $\vec{c}$ è il vettore
\[
\vec{c}=(a+b\cos\theta)\vec{e}_1+b\sin\theta\vec{e}_2.
\]
\paragraph{Somma con metodo algebrico}
Procediamo ora invece con le componenti
\[
	\vec{a}=a\vec{e}_1,\quad \vec{b}=b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2,
\]
le funzioni seno e coseno determinano correttamente il segno delle componenti, quindi non dobbiamo preoccuparcene troppo. Ad esempio, se il vettore $\vec{b}$ si trovasse nel terzo quadrante, seno e coseno avrebbero valori negativi. Ora sommiamo i due vettori seguendo lo stesso procedimento che abbiamo seguito sopra
\[
\vec{c}=\vec{a}+\vec{b}=a\vec{e}_1+(b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2),
\]
facciamo le somme lungo $\vec{e}_1,\vec{e}_2$ e otteniamo
\[
\vec{c}=\underbracket[.5pt]{(a+b\cos\theta)}_{c_1}\vec{e}_1+\underbracket[.5pt]{(b\sin\theta)}_{c_2}\vec{e}_2,
\]
e abbiamo finito.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm,>=latex]
\clip(1,-4) rectangle (11,-1);
\draw [color=gray!30, xstep=1cm,ystep=1cm] (-4.3,-12.08) grid (26.34,6.3);
\begin{scriptsize}
\draw [->,line width=.5pt] (2,-3) -- (10,-3)node[below]{$x$};
\draw [->,line width=1pt,draw=Crimson] (2,-3) --node[below,midway]{$\vec{v}$} (6,-3);
\draw [->,line width=1pt,draw=DarkSlateBlue] (6,-3) -- node[below,midway]{$\vec{w}$}(8,-3);

\draw[fill=black,->,line width=1pt] (2,-2)--node[midway,above]{$\vec{e}_1$} (3,-2);
\draw [fill=Black] (2,-3) circle (1pt) node[below] {$O$};
\draw[decoration={brace,mirror,raise=10pt},decorate]
  (2,-3) -- node[below=12pt,sloped] {$v$} (6,-3);
  \draw[decoration={brace,mirror,raise=10pt},decorate]
  (6,-3) -- node[below=12pt,sloped] {$w$} (8,-3);
  \draw[decoration={brace,mirror,raise=5pt},decorate]
  (8,-3) -- node[below=-18pt,sloped] {$v+w$} (2,-3);
\end{scriptsize}
\end{tikzpicture}
\caption{Somma di due vettori lungo la stessa direzione.}
\end{figure}
\begin{app}
Anche se sembra banale, si deve dimostrare che
\[
v\vec{e}_1\pm w\vec{e}_1=(v\pm w)\vec{e}_1.
\]
La dimostrazione è altrettanto banale, infatti: il modulo del vettore a sinistra è uguale al modulo del vettore a destra, la direzione è la stessa per costruzione ed il verso è dato in entrambi i casi dal segno di $v\pm w$.\qed
\end{app}
\begin{app}(Facoltativo)
A questo punto, potremmo sostituire i valori numerici delle costanti $a,b,\theta$ e riesprimere il vettore nella forma ``modulo, direzione, verso'' utlizzando il Teorema di Pitagora e la funzione inversa alla tangente. Proviamo invece a fare i conti tenendo $a,b,\theta$. Il modulo al quadrato del vettore si esprime come
\[
\begin{split}
c^2& =(a+b\cos\theta)^2+(b\sin\theta)^2\\
&=a^2+2ab\cos\theta+b^2\cos^2\theta+b^2\sin^2\theta\\
&=a^2+b^2\underbracket[.5pt]{(\cos^2\theta+\sin^2\theta)}_{=1}+2ab\cos\theta\\
&=a^2+b^2+2ab\cos\theta.
\end{split}
\]
Se invece di $\theta=\ang{45}$ avessimo $\theta=\ang{90}$, il coseno si annullerebbe
\[
\cos\ang{90}=0
\]
e avremmo
\[
c^2=a^2+b^2,
\]
ovvero otteniamo il Teorema di Pitagora come caso particolare. 

Rimane da determinare l'angolo orientato che determina direzione e verso del vettore: a seconda del segno delle componenti del vettore somma, possiamo stabilire in quale quadrante questo si trova; nel caso specifico, le componenti sono entrambe positive ed il vettore si trova nel primo quadrante, quindi l'inversa della tangente restituisce l'ampiezza dell'angolo formato da $\vec{c}$ con l'asse $x$ in senso antiorario
\[
	\gamma=\tan^{-1}\Big(\frac{b\sin\theta}{a+b\cos\theta}\Big).
\]
Riassumendo, il modulo, la direzione ed il verso del vettore somma sono dati da
\[
\begin{split}
c&=\sqrt{a^2+b^2+2ab\cos\theta}\\
\gamma&=\begin{cases}
\tan^{-1}\Big(\frac{b\sin\theta}{a+b\cos\theta}\Big),&c_1>0,c_2>0\\
180+\tan^{-1}\Big(\frac{b\sin\theta}{a+b\cos\theta}\Big),&c_1<0,c_2>0\\
180+\tan^{-1}\Big(\frac{b\sin\theta}{a+b\cos\theta}\Big),&c_1<0,c_2<0\\
360+\tan^{-1}\Big(\frac{b\sin\theta}{a+b\cos\theta}\Big),& c_1>0,c_2<0\\
\end{cases}
\end{split}
\]\qed
\end{app}
Torniamo all'esercizio (sostituiamo i numeri dell'espressione delle componenti), 
\[
\vec{c}=\SI{19.07}{m}\vec{e}_1+\SI{7,07}{m}\vec{e}_2,
\]
da cui
\[
c=\sqrt{(\SI{19.07}{m})^2+(\SI{7.07}{m})^2}=\SI{20,34}{m},\quad \gamma=\tan^{-1}\Big(\frac{\SI{7,07}{m}}{\SI{19.07}{m}}\Big)=\ang{20.3}.
\]
\begin{eg}
Consideriamo la somma
\[
\vec{c}=\vec{a}+\vec{b}
\]
dei vettori
\[
\begin{split}
&\vec{a}:\quad a=\SI{20}{m},\quad\text{lungo l'asse }x,\quad\text{verso positivo},\\
&\vec{b}:\quad b=\SI{50}{m},\quad\text{forma un angolo }\theta=\ang{210}\text{ con l'asse }x\text{ in senso antiorario}.
\end{split}
\]
Riscriviamo i vettori da sommare in componenti
\[
\begin{split}
\vec{a}&=a\vec{e}_1,\\
\vec{b}&=b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2,
\end{split}
\]
sommiamo
\[
\vec{c}=\vec{a}+\vec{b}=a\vec{e}_1+b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2=(a+b\cos\theta)\vec{e}_1+b\sin\theta\vec{e}_2,
\]
sostituiamo i numeri
\[
\vec{c}=\SI{-23,30}{m}\vec{e}_1+(\SI{-25}{m})\vec{e}_2,
\]
da cui calcoliamo modulo, direzione e verso
\[
\begin{split}
c&=\sqrt{(\SI{-23,30}{m})^2+(\SI{-25}{m})^2}=\SI{34,17}{m},\\
\gamma&=\tan^{-1}\Big(\frac{\SI{-25}{m}}{\SI{-23,30}{m}}\Big)+\ang{180}=\ang{227.02}.
\end{split}
\]
Chiaramente, troncando i valori numerici delle componenti si introducono degli \textbf{errori}.
\end{eg}
\begin{eg}
Sommare
\[
\begin{split}
&\vec{a}:\quad a=\SI{30}{m},\quad\text{direzione come l'asse }x,\quad\text{verso positivo dell'asse }x,\\
&\vec{b}:\quad b=\SI{30}{m},\quad\text{forma un angolo }\theta=\ang{275}\text{ in senso antiorario con l'asse }x
\end{split}
\]
Scriviamo le componenti
\[
\vec{a}=a\vec{e}_1,\quad\vec{b}=b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2,
\]
quindi
\[
\vec{c}=\vec{a}+\vec{b}=a\vec{e}_1+b\cos\theta\vec{e}_1+b\sin\theta\vec{e}_2=(a+b\cos\theta)\vec{e}_1+b\sin\theta\vec{e}_2,
\]
sostituiamo i numeri
\[
\vec{c}=\SI{32,61}{m}\vec{e}_1+(\SI{-29,89}{m})\vec{e}_2,
\]
calcoliamo modulo, direzione e verso
\[
c=\sqrt{(\SI{32,61}{m})^2+(\SI{-28.89}{m})^2}=\SI{44,24}{m},\quad\gamma=\tan^{-1}\Big(\frac{\SI{-28,89}{m}}{\SI{32,61}{m}}\Big)+\ang{360}=\ang{317,49}.
\]
\end{eg}

\begin{figure}[!ht]
\centering
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm,>=latex,scale=.3]
\clip(-1,-3) rectangle (52,10);
\begin{scriptsize}

\draw [color=gray!30, xstep=3cm,ystep=3cm] (-5.14,-18.55) grid (56.14,19.01);
\draw [shift={(10,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) --(0:4) arc (0:45:4) -- cycle;
\draw [shift={(25,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:3.4) arc (0:45:3.4) -- cycle;
\draw [shift={(34,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:4) arc (0:45:4) -- cycle;
\draw [shift={(40,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:4) arc (0:45:4) -- cycle;
%\draw[dash pattern=on 2pt off 2pt,line width=.5pt,draw=black] (-5,0)--(100,0);
\draw [shift={(10,0)},->,line width=.5pt,color=black] (0:4) arc (0:45:4)node[midway,left]{$\theta$};
\draw [shift={(25,0)},->,line width=.5pt,color=black] (0:3.4) arc (0:45:3.4)node[midway,left]{$\theta$};
\draw [shift={(34,0)},->,line width=.5pt,color=black] (0:4) arc (0:45:4)node[midway,left]{$\theta$};
\draw [shift={(40,0)},->,line width=.5pt,color=black] (0:4) arc (0:45:4)node[midway,left]{$\theta$};
\draw [shift={(0,0)},line width=.5pt,color=black,fill=black,opacity=0.10] (0,0) -- (0:4) arc (0:30.37:4) -- cycle;
\draw [shift={(0,0)},->,line width=.5pt,color=black] (0:4) arc (0:30.37:4)node[midway,left]{$\rho$};
\draw [->,line width=.75pt,draw=Crimson] (0,0) -- node[sloped,below,midway] {$\vec{a}$}(10,0);
\draw [->,line width=.75pt,draw=DarkSlateBlue] (10,0) -- node[sloped,midway,above] {$\vec{b}$} (15.65685424949238,5.65685424949238);
\draw [->,line width=.75pt,draw=SeaGreen] (15.65685424949238,5.65685424949238) -- node[midway,sloped,above]{$\vec{c}$} (9.65685424949238,5.65685424949238);

\draw [->,line width=.75pt,draw=Crimson] (15,0) -- node[midway,sloped,below]{$\vec{a}$}(25,0);
\draw [->,line width=.75pt,draw=SeaGreen] (25,0) -- node[midway,sloped,above]{$\vec{c}$}(19,0);
\draw [->,line width=.75pt,draw=DarkSlateBlue] (25,0) -- node[midway,sloped,above]{$\vec{b}$}(30.65685424949238,5.65685424949238);
\draw [->,line width=.75pt,draw=SeaGreen,opacity=.5] (30.65685424949238,5.65685424949238) -- node[midway,sloped,above]{$\vec{c}$}(24.656854249492376,5.65685424949238);

\draw [->,line width=.75pt,draw=SaddleBrown] (30,0) -- node[midway,sloped,below]{$\vec{a}+\vec{c}$}(34,0);
\draw [->,line width=.75pt,draw=DarkSlateBlue] (34,0) -- node[midway,sloped,above]{$\vec{b}$}(39.65685424949238,5.65685424949238);
\draw [->,line width=.75pt,draw=SaddleBrown] (40,0) -- node[midway,sloped,below]{$\vec{a}+\vec{c}$}(44,0);
\draw [->,line width=.75pt,draw=DarkSlateBlue] (40,0) -- node[midway,sloped,above]{$\vec{b}$}(45.65685424949238,5.65685424949238);

\draw [line width=.5pt, dash pattern=on 2pt off 2pt] (24.656854249492376,5.65685424949238)-- (19,0);
\draw [line width=.5pt, dash pattern=on 2pt off 2pt] (45.65685424949238,5.65685424949238)-- (49.65685424949238,5.65685424949238);

\draw [->,line width=.75pt,draw=DarkSlateBlue,opacity=.5,] (44,0) --node[midway,sloped,above]{$\vec{b}$} (49.65685424949238,5.65685424949238);
\draw [->,line width=.75pt,draw=Black] (0,0) -- node[sloped,above,midway] {$\vec{R}$}(9.65685424949238,5.65685424949238);

\draw [fill=black] (25,0) circle (2.5pt) node[below] {$P$};
\draw [fill=black] (30.65685424949238,5.65685424949238) circle (2.5pt) node[right] {$Q$};
\draw [fill=black] (24.656854249492376,5.65685424949238) circle (2.5pt);
\draw [fill=black] (19,0) circle (2pt);
\draw [fill=black] (40,0) circle (2.5pt)  node[left] {$O$};
\draw [fill=black] (44,0) circle (2.5pt) node [right]{$P$};
\draw [fill=black] (45.65685424949238,5.65685424949238) circle (2.5pt);
\draw [fill=black] (49.65685424949238,5.65685424949238) circle (2.5pt);
\end{scriptsize}
\end{tikzpicture}
\caption{Calcolo del risultate di tre vettori.}
\end{figure}
\begin{eg}
Sono dati tre vettori consecutivi
\[
\begin{split}
&\vec{a}:\quad a=\SI{100}{m},\quad\text{direzione come l'asse }x,\quad\text{verso positivo dell'asse }x,\\
&\vec{b}:\quad b=\SI{80}{m},\quad\text{forma un angolo }\theta=\ang{45}\text{ in senso antiorario con l'asse }x,\\
&\vec{c}:\quad c=\SI{60}{m},\quad\text{direzione come l'asse x},\text{verso negativo dell'asse }x,
\end{split}
\]
si chiede di calcolare il vettore risultante $\vec{R}$ (chiamiamo $\rho$ l'angolo orientato in senso antiorario che forma con l'asse $x$).

Invece di mettersi a fare le somme alla cieca, ragioniamo: il vettore $\vec{a}$ è diretto come il vettore $\vec{c}$ (ovvero giacciono su rette parallele), quindi possiamo trasportare $\vec{c}$ parallelalmente a se stesso, muovendo il suo primo estremo $Q$ su $P$; ora sommiamo i due vettori $\vec{a}$ e $\vec{c}$
\[
\vec{a}+\vec{c}=(a-c)\vec{e}_1,
\]
e spostiamo parallelamente a se stesso il vettore $\vec{b}$ muovendo il suo primo estremo $P$ su $O$. In questo modo abbiamo ridotto un esercizio apparentemente complicato alla somma di due vettori
\[
\begin{split}
&\vec{v}\coloneqq\vec{a}+\vec{c}:\quad v= a-c=\SI{40}{m},\quad\text{direzione come l'asse }x,\quad\text{verso positivo dell'asse }x,\\
&\vec{b}:\quad b=\SI{80}{m},\quad\text{forma un angolo }\theta=\ang{45}\text{ in senso antiorario con l'asse }x.
\end{split}
\]
Calcoliamo la somma di questi due vettori come abbiamo imparato
\[
\vec{v}=\SI{40}{m}\vec{e}_1,\quad \vec{b}=\SI{80}{m}(\cos\ang{45}\vec{e}_1+\sin\ang{45}\vec{e}_2),
\]
quindi
\[
\begin{split}
\vec{v}+\vec{b}&=\SI{40}{m}\vec{e}_1+\SI{80}{m}(\cos\ang{45}\vec{e}_1+\sin\ang{45}\vec{e}_2)\\
&=\SI{40}{m}(1+\sqrt2)\vec{e}_1+40\sqrt{2}\si{m}\vec{e}_2\\
&=\SI{96.57}{m}\vec{e}_1+\SI{56.57}{m}\vec{e}_2.
\end{split}
\]
Otteniamo
\[
R=\norm{\vec{a}+\vec{b}+\vec{c}}=\sqrt{(\SI{96.6}{m})^2+(\SI{56.6}{m})^2}=\SI{111,92}{m},\quad \rho=\tan^{-1}\Big(\frac{\SI{56.57}{m}}{\SI{96.57}{m}}\Big)=\ang{30.4}.
\]
\end{eg}
\begin{eg}
Sommare i vettori $\vec{a}$ e $\vec{b}$ che formano con l'asse $x$ angoli orientati in senso antiorario $\alpha$ e $\beta$ rispettivamente
\[
\begin{split}
&\vec{a}:\quad a=\SI{70}{N},\quad \alpha=\ang{30},\\
&\vec{b}:\quad b=\SI{90}{N},\quad\beta=\ang{170}.
\end{split}
\]
I vettori si scrivono in componenti come
\[
\begin{split}
\vec{a}&=a\cos\alpha\vec{e}_1+a\sin\alpha\vec{e}_2,\\
\vec{b}&=b\cos\beta\vec{e}_1+b\sin\beta\vec{e}_2,
\end{split}
\]
sommiamo
\[
\begin{split}
\vec{a}+\vec{b}&=(a\cos\alpha+b\cos\beta)\vec{e}_1+(a\sin\alpha+b\cos\beta)\vec{e}_2\\
&=-\SI{28.01}{N}\vec{e}_1+\SI{50.63}{N}\vec{e}_2,
\end{split}
\]
da cui possiamo calcolare modulo, direzione e verso
\[
\norm{\vec{a}+\vec{b}}=\SI{57.86}{N},\quad \gamma=\ang{119}.
\]
\end{eg}
\end{document}
