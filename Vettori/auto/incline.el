(TeX-add-style-hook
 "incline"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "pgfplots"
    "mathrsfs")
   (TeX-add-symbols
    '("answer" 1)
    "degre"))
 :latex)

