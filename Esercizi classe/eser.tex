\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}

\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother

\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}

\newtheoremstyle{rmk}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{rmk}
\newtheorem{exc}{Esercizio}

\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{section}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\newcommand{\emptybox}[2][\textwidth]{%
  \begingroup
  \setlength{\fboxsep}{-\fboxrule}%
  \noindent\framebox[#1]{\rule{0pt}{#2}}%
  \endgroup
}


\title{Esercizi su calori specifici e temperature di equilibrio}
\author{Alessandro Rossi}

\begin{document}
\maketitle

\renewcommand{\proofname}{Soluzione}
\begin{exc}
Un proiettile di piombo ($c_\pd{lead}=\SI{129.8}{\joule\per\kilo\gram\per\kelvin}$), avente velocità $v=\SI{200}{\meter\per\second}$, penetra in un blocco di legno fisso e si ferma; la temperatura iniziale del proiettile è $T_0=\SI{20}{\celsius}$. Se si ammette che \textbf{tutta} l'energia persa dal proiettile provochi un aumento della temperatura del proiettile $\Delta T$, determinare: la variazione $\Delta T$; se il proiettile fonde ($T_\pd{fus}=\SI{600}{\kelvin}$). 
\end{exc}
\begin{proof}
Il testo dell'esercizio ci dice che tutta l'energia cinetica del proiettile viene trasformata in calore che viene assorbito dal proiettile
\[
Q=\frac12 mv^2=mc_\pd{lead}\Delta T,
\]
da cui deriviamo
\[
\Delta T=\frac{v^2}{2c_\pd{lead}}=\frac{(\SI{200}{\meter\per\second})^2}{2\times\SI{129.8}{\joule\per\kilo\gram\per\kelvin}}=\SI{154}{\celsius}.
\]
La temperatura di fusione del piombo in gradi Celsius è $T_\pd{fus}=\SI{326.85}{\celsius}$, quindi il proiettile non fonde poiché la sua temperatura finale è
\[
T=T_0+\Delta T=T_0+\frac{v^2}{2c_\pd{lead}}=\SI{20}{\celsius}+\SI{154}{\celsius}=\SI{174}{\celsius}<T_\pd{fus}.
\]
\end{proof}
\begin{exc}[Walker-30]
In un calorimetro di piccola capacità termica che contiene \SI{178}{\gram} di acqua a \SI{24.5}{\celsius} poni una palla di piombo di \SI{223}{\gram} con temperatura di \SI{83,2}{\celsius}. Trovare la temperatura di equilibrio del sistema.
\end{exc}
\begin{proof}
Chiamiamo $T_\pd{w},T_\pd{l}$ le temperature iniziali dell'acqua e della palla di piombo rispettivamente.
Anzitutto osserviamo che il contenuto del calorimetro non è in grado di scambiare calore con l'esterno, infatti: se ci fosse un passaggio di calore $Q$ dall'ambiente esterno al contenitore (la cui capacità termica è $C\approx0$), questo sarebbe pari a
\[
	Q=C\Delta T\approx 0,
\]
quindi sarebbe del tutto trascurabile: il sistema acqua + palla di piombo è \emph{adiabatico}. Questo significa che gli unici scambi di calore possibili all'interno del calorimetro sono tra la palla e l'acqua, e poiché il calore netto scambiato con l'ambiente esterno è $0$
\[
Q_1+Q_2=0,
\]
dove
\[
Q_1=m_\pd{w}c_\pd{w}(T-T_\pd{w}),\quad Q_2=m_\pd{l}c_\pd{l}(T-T_\pd{l}).
\]
Ci aspettiamo che la temperatura di equilibrio sia $T_\pd{w}<T<T_\pd{l}$, quindi $Q_1>0,Q_2<0$, ovvero la palla cede calore all'acqua che lo acquista. Dal bilancio energetico determiniamo la temperatura all'equilibrio $T$

\begin{gather*}
m_\pd{w}c_\pd{w}(T-T_w)+m_\pd{l}c_\pd{l}(T-T_\pd{l})=0,\\
T=\frac{m_\pd{w}T_\pd{w}c_\pd{w}+m_\pd{l}T_\pd{l}c_\pd{l}}{m_\pd{w}c_\pd{w}+m_\pd{l}c_\pd{l}}.
\end{gather*}
Sostituendo i dati troviamo
\[
T=\frac{\SI{178}{\gram}\cdot\SI{24.5}{\celsius}\cdot\SI{4186.8}{\joule\per\kilo\gram\per \celsius}+\SI{223}{\gram}\cdot\SI{83.2}{\celsius}\cdot\SI{129.8}{\joule\per\kilo\gram\per\celsius}}{\SI{178}{\gram}\cdot\SI{4186.8}{\joule\per\kilo\gram\per \celsius}+\SI{223}{\gram}\cdot\SI{129.8}{\joule\per\kilo\gram\per\celsius}}=\SI{26.8}{\celsius}.
\]
Per esercizio controllare che sostituendo la temperatura di equilibrio nelle espressioni di $Q_1,Q_2$ i segni sono corretti.
\end{proof}
\begin{exc}[Walker-52]
Un masso di \SI{206}{\kilo\gram} si trova sotto al sole su una scogliera alta \SI{5}{\meter}. La temperatura del masso è \SI{30.2}{\celsius}. Se il masso precipita dalla scogliera in un laghetto contenente \SI{6}{\cubic\meter} di acqua, quale sarà la temperatura finale del sistema acqua-masso? Assumere che il calore specifico del masso sia $c_\pd{r}=\SI{1010}{\joule\per\kilo\gram\per\celsius}$.
\end{exc}
\begin{proof}
Supponiamo che il sistema acqua-masso sia adiabatico, che durante la caduta non intervengano forze dissipative (come ad esempio l'attrito viscoso dell'aria) e che al termine della caduta tutta l'energia cinetica del masso venga convertita in calore. Chiamiamo $T_\pd{w}$ e $T_\pd{r}$ le temperature dell'acqua e del masso rispettivamente. Poiché tutta l'energia cinetica viene convertita in calore del sistema acqua-masso
\[
	Q_1+Q_2=Q=\frac12 m_\pd{r}v^2=m_\pd{r}gh>0,
\]
dove 
\[
Q_1=m_\pd{w}c_\pd{w}(T-T_\pd{w}),\quad Q_2=m_\pd{r}c_\pd{r}(T-T_\pd{r}),
\]
	quindi, l'equazione di bilancio diventa
\[
	m_\pd{w}c_\pd{w}(T-T_\pd{w})+m_\pd{r}c_\pd{r}(T-T_\pd{r})=m_\pd{r}gh,
\]
da cui possiamo determinare $T$. Un altro modo di vederla (forse per voi così è più semplice) è pensare che il contributo di calore ottenuto per conversione di energia meccanica venga \emph{ceduto al} sistema dal masso, quindi deve essere negativo
\[
Q_\pd{mec}=-m_\pd{r}gh<0,
\]
e, sommando questo contributo agli altri due sopra, otteniamo
\[
	\underbracket[.5pt]{Q_1}_{\text{contributo acqua}}+\underbracket[.5pt]{Q_2+Q_\pd{mec}}_{\text{contributo masso}}=0,
\]
ovviamente esplicitando i termini si ottiene la medesima equazione. La temperatura di equilibrio è pertanto
\[
T=\frac{\rho_\pd{w}V_\pd{w}c_\pd{w}T_\pd{w}+m_\pd{r}c_\pd{r}T_\pd{r}+m_\pd{r}gh}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}}=\SI{15.6}{\celsius}.
\]

Proviamo a sbagliare i segni nel bilancio energetico volontariamente per capire cosa accade: il bilancio, scritto coi segni sbagliati, produce una temperatura di equilibrio
\[
T_\pd{wrong}=\frac{\rho_\pd{w}V_\pd{w}c_\pd{w}T_\pd{w}+m_\pd{r}c_\pd{r}T_\pd{r}\mathbf{-m_\pd{r}gh}}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}},
\]
proviamo a confrontare questa temperatura con la temperatura all'equilibrio dello stesso sistema acqua-masso nel quale non avviene la conversione di energia meccanica in calore 
\[
T_{Q=0}=\frac{\rho_\pd{w}V_\pd{w}c_\pd{w}T_\pd{w}+m_\pd{r}c_\pd{r}T_\pd{r}}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}}.
\]
La temperatura scorretta si scrive come
\[
T_\pd{wrong}=\frac{\rho_\pd{w}V_\pd{w}c_\pd{w}T_\pd{w}+m_\pd{r}c_\pd{r}T_\pd{r}}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}}-\frac{m_\pd{r}gh}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}}=T_{Q=0}-\underbracket[.5pt]{\frac{m_\pd{r}gh}{\rho_\pd{w}V_\pd{w}c_\pd{w}+m_\pd{r}c_\pd{r}}}_{>0}<T_{Q=0},
\]
questo significa che la temperatura di equilibrio di un sistema che sta \emph{assorbendo} calore $Q>0$ (il contributo di calore dovuto alla conversione di energia cinetica del masso) è minore della temperatura di equilibrio dello stesso sistema con $Q=0$, il che è chiaramente assurdo.
\end{proof}
\begin{exc}[per casa, Walker-58]
Un orologio a pendolo è costituito di un pendolo semplice di ottone di lunghezza $L=L(T)$. Una notte, la temperatura della casa è di \SI{23}{\celsius} ($T_\pd{i}$) e il periodo ($\tau_0$) del pendolo è di \SI{1}{\second}. A questa temperatura l'orologio funziona correttamente. Se la temperatura alle dieci di sera scende rapidamente a \SI{19}{\celsius} ($T_\pd{f}$) e rimane costante, quale sarà l'ora vera nell'istante in cui l'orologio indicherà le dieci di mattina, il giorno seguente?
\end{exc}
\begin{proof}Per ciascuna richiesta dare la risposta sia in formule che in valore numerico.

\noindent Scrivere la formula per determinare il periodo di un pendolo semplice.\\
\emptybox{2cm}
Qual è la lunghezza del pendolo poco prima delle 10:00 p.m.? ($L\pd{i}=L(T_\pd{i})=\dots$)\\
\emptybox{3cm}
Qual è la lunghezza del pendolo dopo le 10:00 p.m.? ($L_\pd{f}=L(T_\pd{f})=\dots$)\\
\emptybox{3cm}
Qual è il periodo del pendolo ($\tau$) dopo le 10:00 p.m.? ($\tau=\dots$)\\
\emptybox{5cm}
Determinare quanto tempo l'orologio a pendolo perde o guadagna durante una singola oscillazione del pendolo ($\Delta\tau=\dots$).\\
\emptybox{5cm}
\begin{proof}
\[
\Delta\tau=\tau_\pd{i}\Big(\sqrt{1+\alpha_\pd{brass}\Delta T}-1\Big).
\]
Se avete iniziato analisi matematica (altrimenti ignorate il commento) sapete che
\[
\sqrt{1+x}=1+\frac x2 +\mathcal{O}(x^2),\quad x\to0,
\]
poiché $\alpha_\pd{brass}\ll1$ possiamo approssimare la formula sopra come 
\[
\Delta\tau\approx\tau_\pd{i}\Big(1+\frac{\Delta T\alpha_\pd{brass}}{2}-1\Big)=\frac{\tau_\pd{i}\alpha_\pd{brass}\Delta T}{2}.\qedhere
\]
\end{proof}
\noindent Quante oscillazioni compie il pendolo fra le 10:00 p.m. e le 10:00 a.m. (segnate dall'orologio a pendolo)? ($N=\dots$)\\
\emptybox{2cm}
Quanto tempo perde o guadagna l'orologio a pendolo fra le 10:00 p.m. e le 10:00 a.m.? Concludere: qual è l'ora vera alle 10:00 a.m.? ($\Delta t=\dots$)\\
\emptybox{3cm}
\begin{proof}
L'orologio guadagna (ovvero l'ora segnata è avanti rispetto a quella vera, $\Delta\tau<0$) 
\[
\SI{1.64163192}{s},
\]
calcolato con la formula esatta. Calcolando invece con la formula approssimata, l'orologio guadagna
\[
\SI{1.6416}{s},
\]
l'approssimazione è molto buona. L'ora corretta alle 10:00 a.m. è
\[
9:59:58.3584.\qedhere
\]
\end{proof}
\end{proof}
\end{document}

