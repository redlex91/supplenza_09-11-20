\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}

\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother


\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathreplacing,angles,quotes}


\newtheoremstyle{exc}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{exc}
\newtheorem{exc}{Esercizio}

%\theoremstyle{exc}
%\newtheorem{eg}{Esempio}[chapter]
\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{section}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\title{Esercizi guidati sulla temperatura}
\date{}
\author{}
\begin{document}
\thispagestyle{empty}
\pagestyle{empty}
\begin{exc}
Tutte le scale termometriche più comuni (\si{\celsius}, \si{\degree F}, \si{\kelvin}, etc.) assumono che la misura di temperatura dipenda \emph{linearmente} da una certa grandezza, detta \emph{caratteristica termometrica}, che viene utilizzata per definire operativamente la grandezza temperatura; come conseguenza, per passare da una scala termometrica ad un'altra, è sufficiente scrivere una relazione lineare nei valori di misura delle temperature nelle due scale, dipendente da due parametri $\alpha$ e $\beta$, che possono essere determinati dalla conoscenza di due temperature particolari utilizzate per tarare le rispettive scale termometriche: i \emph{punti fissi} della scala termometrica (e.g. il punto triplo dell'acqua a pressione atmosferica standard per la scala Kelvin). Supponiamo ad esempio di voler passare da $\si{\celsius}$ a $\si{\degree F}$, chiamiamo $T$ la misura di temperatura in $\si{\celsius}$ e $t$ la misura di temperatura in $\si{\degree F}$, allora
\begin{equation}\label{eq:conv}
	T=\alpha t+\beta,
\end{equation}
sappiamo inoltre che sono stati misurati i seguenti valori per tarare i due termometri
\[
\begin{array}{lcc}
 &T & t\\
\text{fusione} & \SI{0}{\celsius} & \SI{32}{\degree F}\\
\text{ebollizione} & \SI{100}{\celsius} &\SI{212}{\degree F}
\end{array}
\]
\begin{enumerate}
\item sostituire nella formula di conversione~\ref{eq:conv} le due coppie di valori, in modo da ottenere due equazioni lineari in $\alpha$ e $\beta$;
\item ricavare $\alpha,\beta$ (ricavando $\alpha$ dalla prima e sostituendolo nella seconda, ad esempio...);
\item sostituire i valori ottenuti nella formula di conversione;
\item invertire la formula~\ref{eq:conv} per ottenere $t$ in funzione di $T$;
\item quanto misura $T=\SI{25}{\celsius}$ in $\si{\degree F}$?;
\item a quanto corrisponde una variazione di $\SI{100}{\celsius}$ in $\si{\degree F}$? Scrivere 
\[
\Delta T= T-T'
\]
sostituendo a $T$ e $T'$ le espressioni date dalla formula di conversione~\ref{eq:conv} per le temperature $t$ e $t'$ rispettivamente, quindi risolvere
\[
\Delta T=\SI{100}{\celsius}
\]
rispetto all'incognita $\Delta t=t-t'$. E di \SI{171}{\celsius}? 
\item esiste un valore di temperatura che è eguale nelle due scale? Chiamate $T=t=x$ in~\ref{eq:conv} e risolvete l'equazione di primo grado ottenuta rispetto a $x$.
\end{enumerate}
\end{exc}
\begin{exc}
I metalli si dilatano o ritraggono con buona approssimazione in modo lineare rispetto ad una variazione di temperatura, ovvero
\begin{equation}\label{eq:dillin}
 L\pd{f}-L\pd{i}=\alpha L_\pd{i}(T_\pd{f}-T_\pd{i}),
\end{equation}
dove l'indice ``f'' sta per ``finale'' e l'indice ``i'' sta per ``iniziale'', e $\alpha$ è un numero positivo detto \emph{coefficiente di dilatazione lineare} che dipende dal materiale.
\begin{center}
\includegraphics[width=.5\textwidth]{imgmod}
\end{center}
Abbiamo un cilindro di alluminio ed un anello di rame di diametri $D_0=\SI{4.04}{cm}$ e $d_0=\SI{4.00}{cm}$ rispettivamente, entrambi a temperatura $T_\pd{i}=\SI{10}{\celsius}$, e vogliamo dilatare l'anello di rame in modo che il cilindro possa passarci attraverso. 
\begin{enumerate}
\item Dall'equazione~\ref{eq:dillin}, dedurre se occorre scaldare l'anello oppure raffreddarlo ($d$ è il diametro dell'anello dopo la dilatazione, $T_\pd{f}$ la temperatura alla quale dobbiamo portare l'anello): 
\begin{equation}\label{eq:dillin2}
 \Delta d=d-d_0=\alpha_\pd{Cu} d_0(T_\pd{f}-T_\pd{i})=\alpha_\pd{Cu}d_0\Delta T>0,
\end{equation}
come deve essere $\Delta T$? Positivo o negativo? Dal segno di $\Delta T$ dedurre la risposta (non serve fare calcoli, ragionare sul segno del prodotto).
\item esprimere $\Delta T$ in funzione di $\Delta d$ usando~\ref{eq:dillin2} ($\Delta T=\dots$);
\item qual è il minimo valore di $d$ che permette al cilindro di passare attraverso l'anello? (non serve fare calcoli, il valore è già presente nel testo del problema);
\item sostituire il valore di $d$ in $\Delta d=d-d_0$ nella formula ricavata sopra per $\Delta T$;
\item infine esplicitare $T$ in $\Delta T$ e determinare la temperatura cercata ($T=T_0+\dots$); usare $\alpha_{Cu}=\SI{17e-6}{\per\kelvin}$.
\end{enumerate}
\end{exc}
\end{document}
