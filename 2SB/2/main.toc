\babel@toc {italian}{}\relax 
\babel@toc {italian}{}\relax 
\beamer@sectionintoc {1}{Prima di partire...}{3}{0}{1}
\beamer@sectionintoc {2}{Riprendiamo la lezione precedente...}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Quali caratteristiche deve avere un algoritmo?}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Come si descrive un algoritmo?}{10}{0}{2}
\beamer@sectionintoc {3}{Linguaggi informatici}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{Che lingua parlano le macchine?}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Comunicare istruzioni in modo più umano}{13}{0}{3}
\beamer@subsectionintoc {3}{3}{La traduzione dai linguaggi simbolici a quello della macchina}{21}{0}{3}
