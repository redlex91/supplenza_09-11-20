\documentclass[handout,xcolor={svgnames},aspectratio=169]{beamer}

\usepackage{tikz}
\usetikzlibrary{positioning,arrows}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\definecolor{mybeige}{RGB}{235,222,207}

\lstset{ %
  frame=lines,
  numbers=left,
  numberstyle=\color{white},
  keepspaces=true,
  backgroundcolor=\color{Theme!30},   % choose the background color
  basicstyle=\footnotesize,        % size of fonts used for the code
  breaklines=true,                 % automatic line breaking only at whitespace
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keywordstyle=\color{blue},       % keyword style
  stringstyle=\color{mymauve},     % string literal style
}

\usepackage{fontspec}
\usepackage{inputenc}
\usepackage[italian]{babel}
\setsansfont{Montserrat}
\usepackage{tipa}
\usepackage{mathtools}
\usepackage{fontawesome5}

\usetheme{Bergen}
\title{Introduzione alla programmazione}
\subtitle{Linguaggi di programmazione}
\author{Dott. Alessandro Rossi}

\usepackage{lipsum}
\definecolor{Theme}{RGB}{36,36,132}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}
\begin{frame}\frametitle{Sommario della lezione}
  \tableofcontents
\end{frame}
\section{Prima di partire...}
\begin{frame}\frametitle{Brainstorming sulla parola ``programma''}
  Quali \alert{parole chiave} associate al termine ``programma'' dopo la spiegazione?
  \begin{center}\includegraphics[width=.9\textwidth]{prog.eps}
  \end{center}
  \pause
  \begin{exampleblock}{Parole mancanti?}
    Esecuzione, esecutore, codice, codifica, generalità,...
  \end{exampleblock}  
\end{frame}
\begin{frame}\frametitle{Brainstorming sulla parola ``algoritmo''}
  Quali \alert{parole chiave} associate al termine ``algoritmo'' dopo la spiegazione?
  \begin{center}\includegraphics[width=.9\textwidth]{algo.eps}
  \end{center}
  \pause
    \begin{exampleblock}{Parole mancanti?}
    Risolutore, generalità, finitezza,...
  \end{exampleblock}  
\end{frame}
\section{Riprendiamo la lezione precedente...}
\subsection{Quali caratteristiche deve avere un algoritmo?}
\begin{frame}\frametitle{Risolutore ed esecutore}\pause
  Facciamo una distizione fra l'aspetto della \alert{risoluzione} del problema e dell'\alert{esecuzione} dell'algoritmo\pause
  \begin{block}{Chi risolve il problema?}
    La persona che elabora il metodo di soluzione del problema è il \alert{risolutore}
  \end{block}\pause
  \begin{block}{Cosa esegue l'algoritmo risolutivo?}
    La macchina che esegue istruzione dopo istruzione l'algoritmo che implementa il metodo di soluzione è l'\alert{esecutore}
  \end{block}\pause
  L'esecutore è una macchina \alert{non intelligente}: si limita ad eseguire istruzioni elemetari senza capacità critica o di ragionamento autonomo. Riprendendo la battuta inziale: il risolutore intelligente impartisce comandi all'esecutore non intelligente
\end{frame}
\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  Un algoritmo deve essere:\pause
  \begin{itemize}
  \item<2-> generale 
  \item<3-> finito
  \item<4-> realizzabile
  \item<5-> completo
  \item<6-> riproducibile
  \item<7->non ambiguo e preciso
  \end{itemize}\pause\pause\pause\pause\pause
\end{frame}
\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  \begin{block}{Generalità}
    Come già osservato nella definizione, un algoritmo deve applicarsi a una classe di problemi, non a un solo problema specifico
  \end{block}\pause
  \begin{block}{Finitezza}
    L'algoritmo deve essere composto di un numero (ragionevolmente) finito di istruzioni e la sua esecuzione deve concludersi in un numero finito di passi (esecuzione della singola istruzione);\pause in generale, il numero di passi può essere enormemente più elevato del numero di istruzioni, ma in ogni caso deve potersi raggiungere il risultato in un tempo finito
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  \begin{block}{Realizzabilità}
    L'algoritmo deve essere realizzabile dall'esecutore e in particolare comprensibile da parte di quest'ultimo: pertanto deve essere \alert{molto dettagliato} nel senso di essere composto di istruzioni \alert{elementari} (non ulteriormente scomponibili), in modo da garantire che l'esecutore possa realizzare la singola istruzione
  \end{block}\pause
  \begin{block}{Completezza}
    L'algoritmo deve prevedere tutte le \alert{possibilità} che possono verificarsi durante la sua esecuzione, e per ciascuna deve essere definita un'azione da svolgere
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  \begin{block}{Riproducibilità}
    Eseguendo l'algoritmo in diverse occasioni ma sugli stessi dati di partenza deve produrre i medesimi dati finali (risultati)
  \end{block}\pause
  \begin{block}{Non ambiguità}
    Dal momento che l'esecutore non possiede capacità decisionali proprie, le istruzioni devono potersi eseguire in modo univoco
  \end{block}
\end{frame}
\subsection{Come si descrive un algoritmo?}
\begin{frame}\frametitle{La descrizione di un algoritmo}\pause
  Il linguaggio naturale tende ad essere ambiguo e a prestarsi a varie interpretazioni, pertanto per descrivere un algoritmo è necessario ricorrere a un \alert{linguaggio di progetto}, oppure ad altri metodi di descrizione non ambigui: in ogni caso, le istruzioni devono essere eseguite una per volta e nell'ordine in cui sono scritte\pause

  Nella fase di progettazione di un algoritmo si impiegano due modalità di descrizione:\pause
  \begin{itemize}
  \item<4-> il \alert{diagramma a blocchi} o \alert{flow chart}: metodo di tipo grafico
  \item <5-> il linguaggio di progetto o \alert{pseudocodice}:  consiste nella scrittura ordinata delle istruzioni in un formato vicino a quello del linguaggio di programmazione, è semplificato rispetto a questo ed utilizza parole in italiano (o anche inglese)
  \end{itemize}
  \pause
\end{frame}
\section{Linguaggi informatici}
\subsection{Che lingua parlano le macchine?}
\begin{frame}\frametitle{Dall'algoritmo al programma}
  \begin{block}{Un algoritmo può essere eseguito dal computer?}
    \pause
    No, l'algoritmo deve essere trasformato in un insieme di \alert{comandi} (istruzioni elementari) che l'esecutore è in grado di comprendere in modo univoco
  \end{block}
  \pause
  \begin{itemize}
  \item<3->A tale fine si utilizza un \alert{linguaggio artificiale} privo di ambiguità e comprensibile all'esecutore detto \alert{linguaggio di programmazione}
  \item<4-> L'insieme delle istruzioni scritte in un linguaggio di programmazione prende il nome di \alert{programma}
  \end{itemize}
  \pause\pause
  \begin{alertblock}{Un programma} è l'implementazione di un algoritmo in un linguaggio che può essere compreso ed eseguito da un computer; è costituito da una sequenza ordinata di istruzioni e risolve una famiglia di problemi
  \end{alertblock}
\end{frame}
\begin{frame}\frametitle{I computer parlano una lingua difficile!}\pause
  Il processore di un computer può eseguire solo il \alert{linguaggio macchina} (machine code):
  \pause
  \begin{itemize}
  \item<3-> questo è composto da soli 0 e 1 (bit)
  \item<4-> ogni istruzione è composta di tre parti: indirizzo, codice operativo e dato
  \item<5-> dipende dall'architettura hardware
  \end{itemize}
\pause\pause\pause
\begin{center}
  \tikz{
    \node (a) {\ttfamily 1000};
    \node[below = 0pt of a] {\tiny indirizzo};
    \node[right = of a] (b) {\ttfamily 1111};
    \node[below = 0pt of b] {\tiny op code};
    \node[right = of b] (c){\ttfamily 000000000000};
    \node[below = 0pt of c] {\tiny dato};
  }
\end{center}
\pause
Nei primi calcolatori le istruzioni venivano scritte in \alert{codice binario}: le istruzioni erano indistinguibili dai dati su cui operavano poiché entrambi sono sequenze di zeri e uno, il codice era difficile da modificare una volta scritto
\end{frame}
\subsection{Comunicare istruzioni in modo più umano}
\begin{frame}\frametitle{Dal linguaggio macchina a quello umano: l'assembly}
  \pause
  Un primo passo dal linguaggio della macchina (esecutore) a quello dell'uomo (risolutore) si è fatto col linguaggio \alert{assembly} (o linguaggio assemblatore)
  \pause
  \begin{block}{L'assembly}
    è orientato sia alla macchina sia all'utente umano:
  \end{block}
  \pause
  \begin{itemize}
  \item<4-> ogni istruzione è rappresentata da una parola chiave (keyword) comprensibile all'uomo
  \item<5-> la parola chiave è un'etichetta che viene tradotta in linguaggio macchina da un apposito programma (traduttore/assemblatore)
  \item<6-> l'assembly è dipendente dall'architettura hardware
  \end{itemize}
\end{frame}
\begin{frame}\frametitle{Dal linguaggio macchina a quello umano: l'assembly}
  \pause
  \begin{block}{Che aspetto ha un programma in assembly?}
    Un programma in assembly consiste in un \alert{file di testo} che contiene una serie di comandi (e.g. LOAD, STORE, ADD, HALT,...)
  \end{block}
  \pause
  \begin{itemize}
  \item<3-> i comandi, per essere eseguiti, devono essere \alert{tradotti} in linguaggio macchina dall'assemblatore (assembler)
  \item<4-> i comandi sono in corrispondenza 1:1 con le istruzioni in linguaggio macchina ed è quindi una \alert{rappresentazione simbolica} di ques'ultimo (istruzione assembly \tikz{\node[] (a){}; \node[right = 1pt of a](b){}; \draw[<->,thick](a.west)--([xshift=-3pt]b.east);}  istruzione codice macchina)
  \item<5-> ogni processore ``parla'' una propria versione di assembly, poiché è legato all'insieme di istruzioni che il processore può eseguire
  \end{itemize}
\end{frame}
\begin{frame}\frametitle{Dal linguaggio macchina a quello umano: l'assembly}
  \pause
  Gli assembly rappresentano la seconda generazione di linguaggi di programmazione e condividono coi linguaggi di programmazione più vicini a quello umano lo stesso schema operativo
  \pause
  \begin{center}
    \begin{tikzpicture}[>=latex,block/.style={draw=Theme, line width=1pt,text=Theme, rounded corners=2pt,text width=2.5cm,align=center,font=\bfseries}]
      \node[block] (a) {programma sorgente};
      \node[block, right=of a,minimum height=1.5cm,text=white,fill=Theme] (b) {assembler};
      \node[block,right=of b] (c){programma oggetto};
      \node[below=of a,block,draw=white](d){formato testo (stringhe di caratteri)};
      \node[below=of c,block,draw=white] (e) {formato binario (stringhe di 0/1)};
      \draw[->,thick,draw=Theme] (a)--(b);
      \draw[->,thick,draw=Theme] (b)--(c);
      \draw[double equal sign distance,shorten <=3pt,thick, draw=Theme] (a)--(d);
      \draw[double equal sign distance,shorten <=3pt,thick, draw=Theme] (c)--(e);
    \end{tikzpicture}
  \end{center}
\end{frame}
\begin{frame}\frametitle{Programma sorgente e programma oggetto}
 \pause 
  \begin{center}
    \begin{tikzpicture}[>=latex,block/.style={draw=Theme, line width=1pt,text=Theme, rounded corners=2pt,text width=2.5cm,align=center,font=\bfseries}]
      \node[block] (a) {programma sorgente};
      \node[block, right=of a,minimum height=1.5cm,text=white,fill=Theme] (b) {assembler};
      \node[block,right=of b] (c){programma oggetto};
      \node[below=of a,block,draw=white](d){formato testo (stringhe di caratteri)};
      \node[below=of c,block,draw=white] (e) {formato binario (stringhe di 0/1)};
      \draw[->,thick,draw=Theme] (a)--(b);
      \draw[->,thick,draw=Theme] (b)--(c);
      \draw[double equal sign distance,shorten <=3pt,thick, draw=Theme] (a)--(d);
      \draw[double equal sign distance,shorten <=3pt,thick, draw=Theme] (c)--(e);
    \end{tikzpicture}
  \end{center}
  \pause
  \begin{itemize}
  \item<3-> il programma nel suo formato originale alfanumerico è chiamato \alert{programma sorgente} (stringhe di caratteri)
  \item<4-> il programma assemblato in linguaggio macchina è chiamato \alert{programma oggetto} (codice binario)
  \end{itemize}
  \pause
\end{frame}
\begin{frame}\frametitle{Il livello del linguaggio}
  \pause
  \begin{block}{Basso livello}Un linguaggio di programmazione è tanto più a basso livello quanto più è vicino al linguaggio della macchina; un linguaggio a basso livello è:
  \end{block}
  \pause
  \begin{itemize}
  \item<3-> orientato verso la macchina
  \item<4-> adattato al \alert{funzionamento} della macchina
  \end{itemize}
  \pause\pause
  \begin{block}{Alto livello}
    Un linguaggio di programmazione è tanto più ad alto livello quanto più è vicino al linguaggio naturale (parlato dall'uomo); un linguaggio ad alto livello è:
  \end{block}
  \pause
  \begin{itemize}
  \item<6-> orientato verso l'utente umano
  \item<7-> adattato al \alert{ragionamento} umano
  \end{itemize}
  \pause
\end{frame}
\begin{frame}\frametitle{Verso i linguaggi ad alto livello}
  \pause
  Dagli anni '70 in poi sono nati i \alert{linguaggi procedurali} (FORTRAN, COBOL, Pascal, C,...):
  \pause
  \begin{itemize}
  \item<3-> il controllo del sistema diventa \alert{più logico} e \alert{meno meccanico}
  \item<4-> il linguaggio è distaccato dall'hardware e orientato al programmatore
  \item<5-> si differenzia fra il \alert{programma oggetto} o object file (uno o più file intermedi frutto del processo di ``traduzione'') e il \alert{programma eseguibile} o executable (il file finale eseguibile dal computer)
  \end{itemize}
  \pause
\end{frame}
\begin{frame}\frametitle{Efficienza vs maneggiabilità}
  \pause
  \begin{block}{Efficienza} L'assembly, essendo un linguaggio \alert{aderente} alla macchina, consente la completa utilizzazione delle risorse della macchina e risulta pertanto più efficiente rispetto ai linguaggi ad alto livello; tuttavia, richiede la conoscenza dei dettagli costitutivi della macchina e risulta scarsamente intelligibile dall'uomo, pur essendo più semplice del linguaggio macchina: pertanto non è orientato al problema
  \end{block}
  \pause
  \begin{block}{Maneggiabilità}
    I linguaggi ad alto livello sono più vicini al linguaggio naturale: si tratta di linguaggi simbolici che utilizzano un gruppo di \alert{parole chiave} che possono essere combinate secondo una \alert{sintassi}; permettono di scrivere programmi in una forma molto vicina al modo di ragionare dell'uomo. Rispetto al linguaggio umano, sono meno espressivi, più precisi, semplici e poveri, e privi di qualsiasi ambiguità
  \end{block}
\end{frame}

\begin{frame}[fragile]\frametitle{Come appare il codice in un linguaggio ad alto livello (Python 3 \faPython)?}
  \pause
\begin{center}
 \begin{lstlisting}[showspaces=true,language=Python, basicstyle=\scriptsize\ttfamily]
available_toppings = ['mushrooms', 'olives', 'green peppers',#
 'pepperoni', 'pineapple', 'extra cheese']
requested_toppings = ['mushrooms', 'french fries', 'extra cheese']

for requested_topping in requested_toppings:
    if requested_topping in available_toppings:
        print("Adding " + requested_topping + ".")
    else:
        print("Sorry, we don't have " + requested_topping + ".")

print("\nFinished making your pizza!")
\end{lstlisting}
\end{center}
\pause
\begin{itemize}
\item<3-> il linguaggio è vicino a quello naturale (inglese), infatti si può facilmente intuire cosa fa il programma
\item<4-> {\ttfamily if, else, print, for, in} sono esempi di \alert{parole chiave}
\item<5-> i simboli {\ttfamily( ), [ ], :, \#, +, =, ', ''} e l'indentazione sono disposti assieme alle parole chiave seguendo precise regole di \alert{sintassi}
\end{itemize}
\pause\pause
\end{frame}
\subsection{La traduzione dai linguaggi simbolici a quello della macchina}
\begin{frame}\frametitle{Linguaggi interpretati o compilati}
  \pause
  \begin{block}{Traduzione}
    Nei linguaggi ad alto livello le istruzioni corrispondono a più istruzioni in linguaggio macchina e prima di poter essere eseguite, devono essere tradotte in linguaggio macchina secondo due alternative: \alert{interpretazione} o \alert{compilazione}
  \end{block}
  \pause
  \begin{alertblock}{Interpretazione} Il codice sorgente viene trasformato in codice macchina da un programma, l'\alert{interprete}:\pause
    \begin{itemize}
    \item<4-> questo prende una istruzione alla volta, l'analizza in cerca di errori, la traduce in codice macchina e quindi la manda in esecuzione direttamente, creando un'alternanza fra la fase di traduzione e quella di esecuzione (run-time)
    \item<5-> non viene prodotto alcun file eseguibile
    \item<6-> esempi di linguaggi \alert{intepretati} sono il Python 2/3, Basic, JavaScript, ASP, PHP
    \end{itemize}
  \end{alertblock}
  \pause\pause
\end{frame}
\begin{frame}\frametitle{Linguaggi interpretati o compilati}
  \pause
  \begin{alertblock}{Compilazione}
    Il codice sorgente viene trasformato in codice macchina da un programma, il \alert{compilatore}:
    \pause
    \begin{itemize}
    \item<3-> questo converte l'intero codice sorgente in linguaggio macchina mediante una sequenza di attività, durante le quali rileva eventuali errori, producendo un \alert{programma oggetto}
    \item<4->il programma oggetto non può essere direttamente mandato in esecuzione, ma deve essere rielaborato da un secondo programma, il \alert{linker}, che produce un file \alert{eseguibile} (\texttt{*.exe} su Microsoft) (compile-time)
    \item<5-> l'esecuzione può avvenire in qualunque momento una volta conclusa la fase di compilazione (run-time)
    \item<6-> esempi di \alert{linguaggi compilati} sono il C/C++, FORTRAN, COBOL
      \end{itemize}
    \end{alertblock}
    \pause\pause
\end{frame}
\begin{frame}\frametitle{Qual è il migliore linguaggio di programmazione?}
  \pause
  I linguaggi di programmazione svolgono tutti lo stesso compito: ci permettono di istruire il computer a svolgere determinate operazioni e condizionarle a seconda del presentarsi o meno di alcuni eventi.
  \pause
  La scelta dell'uno o dell'altro linguaggio dipende dalla \alert{piattaforma} e dall'obiettivo che si desidera raggiungere:
  \pause
  \begin{itemize}
  \item<4-> per le applicazioni web si utilizzano Java, Python, Ruby,... 
  \item<5-> per applicazioni client che devono essere eseguite su PC si utilizzano linguaggi come il C
  \item<6-> per le simulazioni numeriche il FORTRAN, nonstante la sua età, è ancora molto utilizzato 
    \item<7-> per la creazione di un sito web dinamico si utilizzano l'HTML (mark-up), il CSS (styling) e il PHP (server-side scripting)
    \end{itemize}
    \end{frame}
  \pause\pause
\end{document}

