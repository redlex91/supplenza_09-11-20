\documentclass[xcolor={svgnames},aspectratio=169,handout]{beamer}

\usepackage{tikz}
\usetikzlibrary{positioning,arrows}

\usepackage{fontspec}
\usepackage{inputenc}
\usepackage[italian]{babel}
\setsansfont{Montserrat}
\usepackage{tipa}

\usetheme{Bergen}
\title{Introduzione alla programmazione}
\subtitle{Problemi e algoritmi}
\author{Dott. Alessandro Rossi}

\usepackage{lipsum}
\definecolor{Theme}{RGB}{36,36,132}
\begin{document}
\begin{frame}
\titlepage
\end{frame}
\begin{frame}\frametitle{Sommario della lezione}
  \tableofcontents
\end{frame}
\section{Prima di partire...}
\begin{frame}\frametitle{Brainstorming sulla parola ``programma''}
  Quali \alert{parole chiave} associate al termine ``programma''?
  \begin{center}\includegraphics[width=\textwidth]{programma.eps}
  \end{center}
\end{frame}
\begin{frame}\frametitle{Brainstorming sulla parola ``algoritmo''}
  Quali \alert{parole chiave} associate al termine ``algoritmo''?
  \begin{center}\includegraphics[width=\textwidth]{algoritmo.eps}
  \end{center}
\end{frame}
  
\section{La programmazione informatica}
\subsection{Cos'è un programma?}
\begin{frame}\frametitle{Ripartiamo dalle basi...}\pause
  \begin{block}{Che cos'è un computer?}\pause
    Il computer non è una macchina intelligente che aiuta le persone stupide, anzi è una macchina stupida che funziona solo nelle mani delle persone intelligenti.\\ \flushright-- Umberto Eco
  \end{block}\pause
  \begin{block}{Questa volta seriamente...}\pause
    Un computer è una macchina complessa in grado di \alert{memorizzare} dati ed \alert{eseguire istruzioni}
  \end{block}\pause
  La programmazione informatica (development) si fonda su queste due caratteristiche
\end{frame}
\begin{frame}\frametitle{Che cos'è un programma?}\pause
   Un computer è una macchina \alert{programmabile}:\pause
  \begin{itemize}
  \item<3->non si limita a fare ciò che vi è già nei circuiti elettrici, uscito dalla fabbrica
  \item <4-> l'utente può istruire, ovvero \alert{programmare}, il computer ad eseguire vari tipi di compiti
  \end{itemize}
  \pause\pause
  Per far sì che un computer svolga un determinato compito dobbiamo indicargli le operazioni che deve svolgere attraverso un \alert{programma}
  \pause
  \begin{block}{Un programma}
    è una serie di \alert{istruzioni} che consentono alla macchina di \alert{elaborare} i dati in maniera appropriata
  \end{block}
\begin{center}
\begin{tikzpicture}[>=latex,block/.style={draw=Theme, line width=1pt,text=Theme, rounded corners=2pt,text width=2.5cm,align=center,font=\bfseries}]
  \node[block] (a) {dati iniziali};
  \node[block, right = of a] (b) {programma};
  \node[block,right = of b] (c) {dati finali};
  \draw[thick,color=Theme,->] (a)--(b);
  \draw[thick,color=Theme,->] (b)--(c);
\end{tikzpicture}
\end{center}
\end{frame}
\begin{frame}\frametitle{Un programma è la risposta a un problema}\pause
  Il compito che il computer deve svolgere è in generale la soluzione di un \alert{problema} (matematico, economico, fisico,...)\pause
\begin{block}{Dal problema all'algoritmo}\pause
    L'\alert{analista} (programmatore) studia il problema e individua un \alert{algoritmo risolutivo}: l'algoritmo consiste nell'insieme delle operazioni (istruzioni) che si deve eseguire per raggiungere il risultato
\end{block}
  \pause
  \begin{block}{Dall'algoritmo al programma}\pause
    L'insieme di queste istruzioni prende il nome di \alert{programma}: ogni singola istruzione deve essere scritta secondo regole rigorose, ovvero rispettando una \alert{sintassi}, che le rende correttamente interpretabili dal computer
  \end{block}
  \begin{center}
\begin{tikzpicture}[>=latex,block/.style={draw=Theme, line width=1pt,text=Theme, rounded corners=2pt,text width=2.5cm,align=center,font=\bfseries}]
  \node[block] (a) {problema};
  \node[block, right = of a] (b) {algoritmo};
  \node[block,right = of b] (c) {programma};
  \draw[thick,color=Theme,->] (a)--(b);
  \draw[thick,color=Theme,->] (b)--(c);
\end{tikzpicture}
\end{center}
\pause
\begin{block}{Linguaggio di programmazione}
  L'insieme delle regole sintattiche è parte del \alert{linguaggio di programmazione}, argomento della prossima lezione
\end{block}
\end{frame}
\begin{frame}\frametitle{Programma o procedura?}
  \pause
  \begin{block}{La procedura per fare del tè}
    Per preparare un tè:
  \end{block}
  \pause
    \begin{itemize}
    \item scaldo l'acqua
    \item la verso nella tazza
    \item aggiungo una bustina di tè
    \item aggiungo un cucchiaino di zucchero
    \item mescolo
    \end{itemize}
    \pause
    \begin{alertblock}{Una procedura}
      è una sequenza di comandi, che viene eseguita dall'inzio alla fine secondo l'ordine sequenziale senza \alert{strutture di controllo} (non si interrompe, non si ripete, non è soggetta a decisioni, etc.)
    \end{alertblock}
\end{frame}
\begin{frame}\frametitle{Programma o procedura?}\pause
  \begin{block}{Il programma per fare del tè}
    Per preparare un tè:
  \end{block}
  \pause
    \begin{itemize}
    \item scaldo l'acqua
    \item la verso nella tazza
    \item aggiungo una bustina di tè
      \pause
    \item \alert{ripeto} il seguente \alert{blocco indentato} di istruzioni fino a un massimo di tre volte:
      \begin{itemize}
      \item \alert{domando}: ``aggiungo un cucchiaino di zucchero? (sì/no)''
        \item \alert{ascolto} la risposta
        \item \alert{se} la risposta è ``sì'': aggiungo un cucchiaino di zucchero
      \item \alert{altrimenti}: \alert{interrompo} la ripetizione e salto al passo successivo
     \end{itemize}
    \item mescolo
    \end{itemize}
    \pause
    \begin{alertblock}{Un programma}
      è una serie ordinata di istruzioni, che implementa un algoritmo, soggetta a condizioni, ripetizioni, interruzioni nel quale l'ordine e la quantità di istruzioni \alert{eseguite} dipende dal presentarsi o meno di un certo numero di eventi
    \end{alertblock}
  \end{frame}
  \subsection{Come si risolve un problema?}
\begin{frame}\frametitle{Come si risolve un problema?}
  \pause
  Alla base della scrittura di un programma vi è l'esigenza di risolvere un \alert{problema}
  \begin{block}{Qual è il primo passo per risolvere un problema?}\pause
    Capire il problema (comprensione)! Il primo passo è quello di \alert{capire cosa} si deve risolvere: \alert{fase di analisi} o studio della situazione reale
  \end{block}\pause
  \begin{block}{E il secondo?}\pause
    \alert{Come} risolvere il problema: individuazione della \alert{strategia} risolutiva, ovvero determinare una \alert{procedura logica} per determinare la soluzione del problema
  \end{block}\pause
  \begin{block}{Ed il passo finale?}\pause
    Consiste nel \alert{tradurre} i primi due passi in un programma (programmazione)
    \end{block}
    \pause
\begin{center}
\begin{tikzpicture}[>=latex,block/.style={draw=Theme, line width=1pt,text=Theme, rounded corners=2pt,text width=2.5cm,align=center,font=\bfseries}]
  \node[block] (a) {problema};
  \node[block, right = 1.5cm of a] (b) {idea};
  \node[block,right = 1.5cm of b] (c) {programma};
  \draw[thick,color=Theme,->] (a)--(b)node[midway,above,rotate=90,anchor=center,text width=1.5cm,align=center] {\scriptsize analisi e strategia};
  \draw[thick,color=Theme,->] (b)--(c)node[midway,above,rotate=90,anchor=center,text width=1.5cm,align=center] {\scriptsize program\-mazione};
\end{tikzpicture}
\end{center}    
\end{frame}
\begin{frame}\frametitle{In cosa consiste l'analisi del problema?}\pause
  Consiste nell'affrontare in modo \alert{sistematico} il problema, studiandone i vari aspetti: da situazioni \alert{complesse} a elementi \alert{accessibili}
  \pause
  \begin{block}{Delimitare l'area di interesse}
    più ampia è la portata del problema, maggiori sono le complessità concettuali, progettuali e operative
  \end{block}
  \pause
  \begin{block}{Individuare dati e condizioni di eleggibilità}
    ovvero determinare a quale \alert{insieme di dati} si estendono i risultati e rispetto a \alert{quali caratteristiche} debbano essere inclusi/esclusi (casi limite/particolari)
  \end{block}
  \pause
  \begin{exampleblock}{E.g.} se state analizzando un problema di dinamica che include una molla ideale, vorrete assicurarvi che i dati iniziali producano deformazioni della molla che non violino il comportamento ideale della molla stessa, includendo magari un limite di rottura/deformazione permanente
  \end{exampleblock}
\end{frame}
\begin{frame}{Cosa significa ``comprendere il problema''?}\pause
  Spesso il problema si presenta in modo \alert{confuso} e fuorviante, quindi dobbiamo eliminare le \alert{ambiguità} nella sua formulazione.
  \begin{block}{Come?}\pause
    Dobbiamo ``fare pulizia''
  \end{block}
\pause
\begin{block}{Evidenziando}
  quelli che sono i reali \alert{obiettivi}, le \alert{regole} e i \alert{dati} sia impliciti sia espliciti
\end{block}
\pause
\begin{block}{Eliminando}
  i dettagli \alert{inutili ed ambigui}
\end{block}\pause
A partire dal problema ``ripulito'' si individuano gli elementi essenziali (astrazione) e si costruisce una rappresentazione semplificata (modello) sulla quale si progetta una strategia di risoluzione
\end{frame}

\section{Gli algoritmi}
\subsection{Che cosa è un algoritmo?}
\begin{frame}\frametitle{Che cosa è un algoritmo?}
  \begin{block}{Algoritmi de Numero Indorum} Il termine \alert{algoritmo} deriva dal titolo della traduzione in latino del manoscritto di algebra di Abu Ja'far Muhammad ibn Musa \alert{Al-Khwarizmi}, ritenuto da molti il padre dell'algebra (800 d.C.): algoritmo = storpiatura di Al-Khwarizmi (proveniente dalla città di Khwarazm, Asia Orientale)
\begin{center}\scriptsize
    \url{https://earthobservatory.nasa.gov/images/91544/how-algorithm-got-its-name}
\end{center}
  \end{block}\pause
  Una volta individuata la strategia risolutiva, si mettono insieme le singole operazioni da compiere per risolvere il problema\pause
  \begin{alertblock}{Algoritmo}
L'insieme delle istruzioni che definiscono una sequenza ordinata di operazioni mediante le quali si risolvono i problemi appartenenti ad una certa \alert{classe}
\end{alertblock}
\begin{block}{In inglese}
  si scrive \alert{algorithm} non algorythm, IPA \textipa{/"\ae l.g\super@.rI\dh\super@m/}
\end{block}
\end{frame}

\begin{frame}{Quali condizioni sono necessarie affinché si possa trovare un algoritmo?}\pause
  Un algoritmo è un metodo di elaborazione che applicato a un certo insieme di dati iniziali, produce determinati dati finali, o risultati, pertanto è necessario che il problema che stiamo studiando sia \alert{ben posto}\pause
  \begin{itemize}
  \item<3-> l'\alert{obiettivo} deve essere chiaro
  \item<4-> ci siano un numero sufficiente di \alert{dati} noti in partenza
  \item<5-> il problema sia \alert{risolubile} da parte di chi lo affronta
  \end{itemize}\pause\pause\pause
  Inoltre è importante distinguere il \alert{risultato} (dati finali prodotti) dalla \alert{soluzione} (la strategia/metodo che li produce)
\end{frame}
\subsection{Quali caratteristiche deve avere un algoritmo?}
\begin{frame}\frametitle{Risolutore ed esecutore}\pause
  Facciamo una distizione fra l'aspetto della \alert{risoluzione} del problema e dell'\alert{esecuzione} dell'algoritmo\pause
  \begin{block}{Chi risolve il problema?}
    La persona che elabora il metodo di soluzione del problema è il \alert{risolutore}
  \end{block}\pause
  \begin{block}{Cosa esegue l'algoritmo risolutivo?}
    La macchina che esegue istruzione dopo istruzione l'algoritmo che implementa il metodo di soluzione è l'\alert{esecutore}
  \end{block}\pause
  L'esecutore è una macchina \alert{non intelligente}: si limita ad eseguire istruzioni elemetari senza capacità critica o di ragionamento autonomo. Riprendendo la battuta inziale: il risolutore intelligente impartisce comandi all'esecutore non intelligente
\end{frame}
\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  Un algoritmo deve essere:\pause
  \begin{itemize}
  \item<2-> generale 
  \item<3-> finito
  \item<4-> realizzabile
  \item<5-> completo
  \item<6-> riproducibile
  \item<7->non ambiguo e preciso
  \end{itemize}\pause\pause\pause\pause\pause
\end{frame}
\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  \begin{block}{Generalità}
    Come già osservato nella definizione, un algoritmo deve applicarsi a una classe di problemi, non a un solo problema specifico
  \end{block}\pause
  \begin{block}{Finitezza}
    L'algoritmo deve essere composto di un numero (ragionevolmente) finito di istruzioni e la sua esecuzione deve concludersi in un numero finito di passi (esecuzione della singola istruzione);\pause in generale, il numero di passi può essere enormemente più elevato del numero di istruzioni, ma in ogni caso deve potersi raggiungere il risultato in un tempo finito
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Quali caratteristiche deve avere un algoritmo?}
  \begin{block}{Realizzabilità}
    L'algoritmo deve essere realizzabile dall'esecutore e in particolare comprensibile da parte di quest'ultimo: pertanto deve essere \alert{molto dettagliato} nel senso di essere composto di istruzioni \alert{elementari} (non ulteriormente scomponibili), in modo da garantire che l'esecutore possa realizzare la singola istruzione
  \end{block}\pause
  \begin{block}{Completezza}
    L'algoritmo deve prevedere tutte le \alert{possibilità} che possono verificarsi durante la sua esecuzione, e per ciascuna deve essere definita un'azione da svolgere
    \end{block}
  \end{frame}

  \begin{frame}
    \frametitle{Quali caratteristiche deve avere un algoritmo?}
    \begin{block}{Riproducibilità}
      Eseguendo l'algoritmo in diverse occasioni ma sugli stessi dati di partenza deve produrre i medesimi dati finali (risultati)
    \end{block}\pause
    \begin{block}{Non ambiguità}
      Dal momento che l'esecutore non possiede capacità decisionali proprie, le istruzioni devono potersi eseguire in modo univoco
    \end{block}
  \end{frame}
  \subsection{Come si descrive un algoritmo?}
\begin{frame}\frametitle{La descrizione di un algoritmo}\pause
  Il linguaggio naturale tende ad essere ambiguo e a prestarsi a varie interpretazioni, pertanto per descrivere un algoritmo è necessario ricorrere a un \alert{linguaggio di progetto}, oppure ad altri metodi di descrizione non ambigui: in ogni caso, le istruzioni devono essere eseguite una per volta e nell'ordine in cui sono scritte\pause

  Nella fase di progettazione di un algoritmo si impiegano due modalità di descrizione:\pause
  \begin{itemize}
  \item<4-> il \alert{diagramma a blocchi} o \alert{flow chart}: metodo di tipo grafico
  \item <5-> il linguaggio di progetto o \alert{pseudocodice}:  consiste nella scrittura ordinata delle istruzioni in un formato vicino a quello del linguaggio di programmazione, è semplificato rispetto a questo ed utilizza parole in italiano (o anche inglese)
  \end{itemize}
  \pause
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
