\babel@toc {italian}{}\relax 
\babel@toc {italian}{}\relax 
\beamer@sectionintoc {1}{Prima di partire...}{3}{0}{1}
\beamer@sectionintoc {2}{La programmazione informatica}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Cos'è un programma?}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Come si risolve un problema?}{10}{0}{2}
\beamer@sectionintoc {3}{Gli algoritmi}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Che cosa è un algoritmo?}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Quali caratteristiche deve avere un algoritmo?}{15}{0}{3}
\beamer@subsectionintoc {3}{3}{Come si descrive un algoritmo?}{20}{0}{3}
