\documentclass[a4paper,oneside,openany,extrafontsizes,11pt]{memoir}

\usepackage[no-math]{fontspec}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{microtype}

%%% font spec
% \setromanfont{Vollkorn}[
% Path=./Vollkorn/,
% Extension=.ttf,
% UprightFont=*-Regular,
% ItalicFont=*-Italic,
% BoldFont=*-Bold,
% BoldItalicFont=*-BoldItalic
% ]
\setromanfont{Montserrat}[
Path=./Montserrat/static/,
Extension=.ttf,
UprightFont=*-Regular,
ItalicFont=*-Italic,
BoldFont=*-Bold,
BoldItalicFont=*-BoldItalic
]


% \setromanfont{OpenDyslexic}[
% Path=./OpenDyslexic/,
% Extension=.otf,
% UprightFont=*-Regular,
% ItalicFont=*-Italic,
% BoldFont=*-Bold,
% BoldItalicFont=*-Bold-Italic
% ]

\setsansfont{Lato}[
Path=./Lato/,
Extension=.ttf,
UprightFont=*-Regular,
ItalicFont=*-Italic,
BoldFont=*-Bold,
BoldItalicFont=*-BoldItalic
]
\setsecnumdepth{book}
\setulmarginsandblock{1.5in}{1in}{}
\setlrmarginsandblock{1in}{1in}{*}
\checkandfixthelayout
\usepackage{mathtools}\usepackage[hidelinks]{hyperref}
\usepackage[svgnames]{xcolor}
\definecolor{ThemeBlue}{RGB}{72,61,139}
\definecolor{ThemeGray}{RGB}{72,71,73}

\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning}
\tikzset{flowchart/.style ={
  startstop/.style = {rectangle, rounded corners=10pt, minimum width=1.5cm, minimum height=.7cm,text centered, text=black, draw=black, thick, font=\bfseries},
  io/.style = {trapezium, trapezium stretches body, inner xsep=0pt, outer sep=0pt, trapezium left angle=70, trapezium right angle=-70, minimum width=2cm, minimum height=.7cm, text centered, draw=Navy, font=\bfseries,text=Navy,thick},
  process/.style = {rectangle, minimum width=1.5cm, minimum height=.7cm, text centered, draw=DarkSlateGray,text=DarkSlateGray,font=\bfseries,thick},
decision/.style = {diamond, minimum width=2cm, minimum height=.7cm, text centered, thick, font=\bfseries, draw=Crimson, text=Crimson},
arrow/.style= {thick,->,>=latex}}}
\title{Rappresentazione di algoritmi: diagramma a blocchi}
\author{Alessandro Rossi}
\usepackage{tabularx,multirow}
\usepackage{lipsum}
\begin{document}
\maketitle
Nella precedente lezione abbiamo visto che, per progettare un programma, dobbiamo prima elaborare un algoritmo. L'algoritmo viene descritto in un \emph{linguaggio di progettazione}, come ad esempio mediante un diagrammi a blocchi o flowchart (che ne dà una rappresentazione grafica) oppure mediante uno pseudocodice. In questa lezione ci focalizziamo sul primo metodo di rappresentazione degli algoritmi: il \emph{diagramma a blocchi}.
\section{I blocchi del diagramma}
Un diagramma a blocchi è una descrizione grafica dell'algoritmo, realizzata mediante appositi \emph{blocchi}, che mette in evidenza le sue istruzioni: ogni tipologia di istruzione è rappresentata da dei blocchi di una certa forma convenzionale. I blocchi di \emph{inizio/fine} algoritmo sono della forma
\begin{center}
\tikz[flowchart,node distance=.5cm]{\node[startstop] (a) {inizio};\node[startstop,right=of a](b) {fine};\node[below=of a](bottom){};\draw[arrow,dashed] (a)--(bottom);\node[above = of b](top){};\draw[arrow,dashed](top)--(b);}
\end{center}
mentre i blocchi che rappresentano \emph{azioni} o gruppi di azioni hanno forma rettangolare
\begin{center}\tikz[flowchart,node distance=.5cm]{\node[process](a){$\dots$};\node[below=of a](bottom){};\node[above=of a](top){};\draw[arrow,dashed](top)--(a)--(bottom);}\end{center}

Il computer ha la possibilità di scambiare informazioni con l'utente ricevendo informazioni (input) oppure fornendo informazioni (output) attravero la tastiera e lo schermo rispettivamente; i blocchi che simboleggiano l'input e l'output hanno forma di parallelogrammi
\begin{center}
  \tikz[flowchart,node distance=.5cm]{\node[io] (a){$\dots$};
    \node at ([yshift=5pt,xshift=-5pt]a.east) {\bfseries\color{Navy}i};
    \node[io,right=of a] (b){$\dots$};
    \node at ([yshift=5pt,xshift=-5pt]b.east) {\bfseries\color{Navy}o};
    \node[below=of a](bottom){};\node[above=of a](top){};\draw[arrow,dashed](top)--(a)--(bottom);
    \node[below=of b](bottom){};\node[above=of b](top){};\draw[arrow,dashed](top)--(b)--(bottom);
  }
\end{center}
oppure
\begin{center}
  \tikz[flowchart,node distance=.5cm]{\node[io] (a){input: $\dots$};
    \node[io,right=of a] (b){output: $\dots$};
    \node[below=of a](bottom){};\node[above=of a](top){};\draw[arrow,dashed](top)--(a)--(bottom);
    \node[below=of b](bottom){};\node[above=of b](top){};\draw[arrow,dashed](top)--(b)--(bottom);
  }
\end{center}
Infine, una struttura di \emph{selezione binaria} che modifica l'esecuzione dell'algoritmo a seconda che una certa condizione sia vera o falsa, è rappresentata da un rombo
\begin{center}\tikz[flowchart,node distance=.5cm]{\node[decision](a){$\dots$};    \node[left=of a,yshift=-.5cm](left){};\node[right=of a,yshift=-.5cm](right){};\node[above=of a](top){};\draw[arrow,dashed](top)--(a);\draw[arrow,dashed](a)-|(left);\draw[arrow,dashed](a)-|(right)}\end{center}
I blocchi di selezione consentono di introdurre delle \emph{strutture di controllo} nell'algoritmo, che ne condizionano l'esecuzione a seconda dei dati introdotti.

I vari blocchi del diagramma sono collegati tra loro da frecce secondo alcune regole:
\begin{itemize}
\item all'inizio è possibile seguire una sola direzione e alla fine si può arrivare attraverso un'unica freccia;
\item i blocchi di input, output e azione possiedono una sola freccia entrante ed una sola freccia uscente:
\item il blocco decisionale (selezione binaria) ha una sola freccia entrante e due frecce uscenti.
\end{itemize}
Vediamo un semplice esempio di diagrammi a blocchi.
\begin{center}
\tikz[flowchart,node distance=.5cm]{
\node[startstop] (start) {inizio};
\node[process, below = of start] (init) {$k\leftarrow0$};
\node[right=of init]{inizializzazione};
\node[io, below = of init] (print) {$k$};
\node[right=of print]{stampa: 0,1,2,3,4,5,6,7,8,9};
\node at ([yshift=5pt,xshift=-5pt]print.east) {\bfseries\color{Navy}o};
\node[process,below=of print] (plus) {$k\leftarrow k+1$};
\node[right=of plus]{riassegnazione: 1,2,3,4,5,6,7,8,9,10};
\node[decision,below = of plus] (ask) {$k\geq10?$};
\node[right=of ask]{decisione: no, no, no, no, no, no, no, no, no, sì};
\node[startstop, below = of ask] (end) {fine};
\draw[arrow] (start)--(init);
\draw[arrow] (init)--(print) node[midway] (repeat){};
\draw[arrow] (print)--(plus);
\draw[thick] (ask)--([xshift=-.5cm]ask.west) node (lask){}; 
\draw[arrow] (lask.center) |- (repeat.center) node[near start,left]{no};
\draw[arrow] (plus.south)--(ask);
\draw[arrow] (ask)--(end) node[midway,right]{sì};
}
\end{center}
L'algoritmo rappresentato dal diagramma stampa i numeri da 0 a 9: la struttura di controllo viene usata per interrompere le iterazioni quando la \emph{variabile} $k$ contiene un numero pari a 10.
\section{Le variabili e la loro assegnazione}
Una variabile può essere pensata come un contenitore nel quale è possibile inserire un valore o sostituirlo con un altro dello stesso tipo ed è caratterizzata da:
\begin{itemize}
\item un \emph{identificatore} (il nome del contenitore);
\item un \emph{valore} (il contenuto);
  \item un \emph{tipo} (esempio: stringa, numero, variabile booleana, etc.).
\end{itemize}
Materialmente una variabile corrisponde a una \emph{cella di memoria} nella memoria centrale (RAM) allocata per contenere un certo dato: in definitiva, una \emph{variabile} è un'astrazione della cella di memoria, un contenitore di informazione il cui contenuto varia nel tempo durante l'esecuzione del programma. Il simbolo $\leftarrow$ indica che nella variabile alla sua sinistra viene inserito il dato alla sua destra
\[
  \mathtt{VAR}\leftarrow \mathtt{DATA}
\]
e prende il nome di \emph{simbolo di assegnazione}; un altro simbolo molto usato per l'assegnazione è
\[
  \mathtt{VAR}\coloneqq \mathtt{DATA}
\]
Quando in una variabile si carica un'informazione per la prima volta, si parla di \emph{inizializzazione} e si dice che la variabile è stata \emph{inizializzata}, mentre per tutte le assegnazioni successive di quella medesima variabile si parla di \emph{riassegnazione}. Nella progettazione di un algoritmo mediante un diagramma a blocchi non è necessario indicare il tipo della varibile, è sufficiente scegliere un indicatore e assegnarle dei valori (i tipi a disposizione possono variare dall'uno all'altro linguaggio di programazione). 

\section{La relazione fra esecutore e utente}
Nella progettazione di un diagramma a blocchi (detto anche diagramma di flusso) dobbiamo tenere contro della differenza di ruolo tra l'\emph{esecutore} (il computer) e l'\emph{utente}:
\begin{itemize}
\item l'utente inserisce i dati in ingresso (input) mediante mouse/tastiera e riceve i dati in uscita (output) attraverso lo schermo (o altri dispositivi);
\item l'esecutore riceve i dati in ingresso, li elabora e restituisce dati in uscita all'utente.
\end{itemize}
I diagrammi a blocchi devono essere composti dal punto di vista dell'esecutore, in quanto lo scopo della progettazione è quello di produrre alla fine un programma vero e proprio eseguibile da una macchina.

Tutti i dati, sia quelli in ingresso, sia quelli in uscita, sia quelli intermedi (generati dalla macchina in seguito a una certa azione) ottenuti dall'esecutore durante lo svolgimento del processo risolutivo vanno collocati all'interno di una variabile (ricordate: i computer possono memorizzare dati!) affinché sia accessibile nelle successive istruzioni; ogni dato non assegnato a una variabile viene perduto e non può essere riutilizato! Nella progettazione di un programma, chiedetevi sempre quali sono i dati iniziali, quali quelli finali, e quali quelli intermedi e decidete quante variabili vi serviranno.
\section{Esempio di progettazione di un algoritmo}
Consideriamo il problema: calcolare e visualizzre l'area della superficie di un triangolo.
\paragraph{Fase di analisi}  Il problema richiede di calcolare l'area della superficie di un triangolo, pertanto devono essere note le misure di base e altezza.
\begin{center}
  \begin{tabular}{cc}
    \multicolumn{2}{c}{\bfseries Spefiche funzionali}\\
    \bfseries Dati iniziali & \bfseries Dati finali\\
     $\mathrm{base}$: misura della base &\multirow{2}{3cm}{\centering $\mathrm{area}$: misura dell'area}\\
    $\mathrm{altezza}$: misura dell'altezza&\\
  \end{tabular}
\end{center}
\paragraph{Strategia risolutiva} Le istruzioni necessarie per risolvere il problema sono le seguenti.
\begin{itemize}
\item inizio;
\item acquisisco il valore della base nella variabile $\mathrm{base}$;
\item acquisisco il valore dell'altezza nella variabile $\mathrm{altezza}$;
\item moltiplico il valore di $\mathrm{base}$ per quello dell'$\mathrm{altezza}$ e divido per 2;
\item comunico il valore di $\mathrm{area}$;
\item fine.
\end{itemize}
\paragraph{Algoritmo risolutivo: flowchart}
\begin{center}
  \begin{tikzpicture}[flowchart, node distance=.5cm]
    \node[startstop] (start) {inizio};
    \node[io,below=of start] (base) {$\mathrm{base}$};
    \node[yshift=5pt,xshift=-5pt]at (base.east) {\bfseries \color{Navy}i};
        \node[io,below=of base] (alt) {$\mathrm{altezza}$};
    \node[yshift=5pt,xshift=-5pt]at (alt.east) {\bfseries \color{Navy}i};
\node[process, below=of alt](calc) {$\mathrm{area}\leftarrow\frac12\cdot(\mathrm{base}\cdot\mathrm{altezza})$};⎈
\node[io, below=of calc] (area){$\mathrm{area}$};
    \node[yshift=5pt,xshift=-5pt]at (area.east) {\bfseries \color{Navy}o};
\node[startstop,below=of area] (stop) {fine};
\draw[arrow] (start)--(base);
\draw[arrow] (base)--(alt);
\draw[arrow] (alt)--(calc);
\draw[arrow] (calc)--(area);
\draw[arrow] (area)--(stop);
\end{tikzpicture}
\end{center}
L'algoritmo appena illustrato risolve il problema del calcolo dell'area della superficie del triangolo ma presenta un problema in quanto l'utente non è in grado di sapere cosa sta facendo l'esecutore: questo rimarrebbe in attesa per sempre in attesa dell'introduzione del valore nella prima variabile. Pertanto, all'interno della strategia risolutiva, è necessario includere delle azioni di \emph{dialogo} tra l'esecutore e l'utente, mediante opportune istruzioni di input e output. 
\begin{center}
  \begin{tikzpicture}[flowchart, node distance=.5cm]
    \node[startstop] (start) {inizio};
    \node[io,below=of start,text width=5cm] (out1) {``Questo programma calcola l'area di un triangolo, noti base e altezza''};
    \node[yshift=15pt,xshift=-3pt]at (out1.east) {\bfseries \color{Navy}o};
    \node[io,below=of out1,text width=5cm] (out2) {``Introdurre il valore della base''};
    \node[yshift=7pt,xshift=-5pt]at (out2.east) {\bfseries \color{Navy}o};
    \node[io,below=of out2] (base) {$\mathrm{base}$};
    \node[yshift=5pt,xshift=-5pt]at (base.east) {\bfseries \color{Navy}i};
    \node[io,below=of base,text width=5cm] (out3) {``Introdurre il valore dell'altezza''};
    \node[yshift=7pt,xshift=-5pt]at (out3.east) {\bfseries \color{Navy}o};
    \node[io,below=of out3] (alt) {$\mathrm{altezza}$};
    \node[yshift=5pt,xshift=-5pt]at (alt.east) {\bfseries \color{Navy}i};
\node[process, below=of alt](calc) {$\mathrm{area}\leftarrow\frac12\cdot(\mathrm{base}\cdot\mathrm{altezza})$};⎈
\node[io, below=of calc] (area){``L'area è'', $\mathrm{area}$};
    \node[yshift=5pt,xshift=-5pt]at (area.east) {\bfseries \color{Navy}o};
\node[startstop,below=of area] (stop) {fine};
\draw[arrow] (start)--(out1);
\draw[arrow] (out1)--(out2);
\draw[arrow] (out2)--(base);
\draw[arrow] (base)--(out3);
\draw[arrow] (out3)--(alt);
\draw[arrow] (alt)--(calc);
\draw[arrow] (calc)--(area);
\draw[arrow] (area)--(stop);
\end{tikzpicture}
\end{center}
Questa app online (non serve scaricare nulla!) \url{diagrams.net} permette di realizzare semplici diagrammi di flusso per esercitarvi.
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
