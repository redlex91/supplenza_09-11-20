\documentclass[12pt,final]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[no-math]{fontspec}

\usepackage[italian]{babel}
\usepackage{microtype}
\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{mathtools}
\DeclarePairedDelimiter{\norm}{\lvert}{\rvert}

\makeatletter
\DeclareFontEncoding{LS1}{}{}
\DeclareFontSubstitution{LS1}{stix}{m}{n}
\DeclareMathAlphabet{\mathscr}{LS1}{stixscr}{m}{n}
\makeatother


\usepackage{siunitx}
\usepackage[svgnames]{xcolor}
\usepackage[colorlinks=true,linkcolor=DarkSlateBlue]{hyperref}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathreplacing,angles,quotes}


\newtheoremstyle{exc}
{\topsep}
{\topsep}
{\normalfont}
{0pt}
{\color{DarkSlateBlue}\bfseries\sffamily}
{}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{exc}
\newtheorem{exc}{Esercizio}

%\theoremstyle{exc}
%\newtheorem{eg}{Esempio}[chapter]
\newcommand{\pd}[1]{{\scriptscriptstyle\textrm{#1}}}
\renewcommand{\vec}{\bm}

\setsecnumdepth{section}

\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\setlrmarginsandblock{.625in}{.75in}{*}
\setulmarginsandblock{.625in}{1.25in}{*}
\checkandfixthelayout
\chapterstyle{culver}

\title{Esercizi su temperatura e calore}
\date{}
\begin{document}
\thispagestyle{empty}
\maketitle
\pagestyle{empty}
\paragraph{Istruzioni}{\itshape Risolvere i problemi sostituendo i dati solo alla fine (o comunque il più tardi possibile). Commentare i passaggi logici cosicché possa capire cosa state facendo. Inviare le soluzioni a \href{mailto:alex.rossi@protonmail.com}{alex.rossi@protonmail.com} con oggetto ``compiti fisica 4sA - Nome Cognome'' entro le 23:99 di Sabato 21/11.}
\begin{exc}[scale termometriche]
Determinare la formula di conversione da gradi Celsius a gradi Fahrenheit sapendo che
\[
T_\pd{eb}=\SI{100}{\celsius}=\SI{212}{\degree F},\quad T_\pd{sol}=\SI{0}{\celsius}=\SI{32}{\degree F}.
\]
Esiste una temperatura che ha lo stesso valore nelle due scale termometriche? Se sì, determinarla. La variazione di \SI{1}{\celsius} corrisponde alla variazione di \SI{1}{\degree F}? Motivare la risposta. {\itshape Suggerimento per determinare la formula di conversione: entrambe le scale termometriche sono costruite supponendo che la temperatura dipenda linearmente dalla caratteristica termometrica (esempio: altezza della colonnina di mercurio), quindi...}
\end{exc}

\begin{exc}[dilatazione termica]
Un orologio a pendolo è costituito di un pendolo semplice di ottone di lunghezza $L=L(T)$. Una notte, la temperatura della casa è di \SI{23}{\celsius} ($T_\pd{i}$) e il periodo ($\tau_0$) del pendolo è di \SI{1}{\second}. A questa temperatura l'orologio funziona correttamente. Se la temperatura alle dieci di sera scende rapidamente a \SI{19}{\celsius} ($T_\pd{f}$) e rimane costante, quale sarà l'ora vera nell'istante in cui l'orologio indicherà le dieci di mattina, il giorno seguente? Usare il coefficiente di dilatazione lineare $\alpha_\pd{brass}=\SI{1.9e-5}{\per\kelvin}$. {\itshape Suggerimento: vi occorre la formula del periodo del pendolo semplice, quindi considerate la variazione di periodo $\Delta\tau=\tau-\tau_0$ e da questa determinate se l'orologio perde o guadagna tempo durante l'intero periodo temporale tra le 10:00 p.m. e le 10:00 a.m.}
\end{exc}

\begin{exc}[su dilatazione termica e temperatura di equilibrio]Si consideri un anello di rame a temperatura $\SI{0}{\celsius}$ di massa $m\pd{Cu}=\SI{20}{\gram}$ e diametro interno $D=\SI{2.54000}{cm}$; su questo viene appoggiata una sfera di alluminio di massa $m_\pd{Al}$ e diametro $d=\SI{2.54508}{cm}$ a temperatura $\SI{100}{\celsius}$. Si assuma che il sistema sfera + anello si porti all'equilibrio termico senza dispersione di calore: nel momento in cui i due oggetti raggiungono l'equilibrio termico, la sfera di alluminio è in grado di passare attraverso l'anello di rame. Determinare la massa della sfera di alluminio. Usare i calori specifici $c_\pd{Cu}=\SI{387}{\joule\per\kilo\gram\per\kelvin},c_\pd{Al}=\SI{896}{\joule\per\kilo\gram\per\kelvin}$ e i coefficienti di dilatazione lineare $\alpha_\pd{Cu}=\SI{1.7e-5}{\per\kelvin},\alpha_\pd{Al}=\SI{2.4e-5}{\per\kelvin}$.
\begin{center}\includegraphics[width=.3\textwidth]{dilat.png}
\end{center}
\end{exc}

\begin{exc}[Calore specifico, calorimetria, equivalenza energia meccanica/calore]
Un masso di \SI{206}{\kilo\gram} si trova sotto al sole su una scogliera alta \SI{5}{\meter}. La temperatura del masso è \SI{30.2}{\celsius}. Se il masso precipita dalla scogliera in un laghetto contenente \SI{6}{\cubic\meter} di acqua a \SI{15.5}{\celsius}, quale sarà la temperatura finale del sistema acqua-masso? Assumere che il calore specifico del masso sia $c_\pd{r}=\SI{1010}{\joule\per\kilo\gram\per\celsius}$, quello dell'acqua $c_\pd{w}=\SI{4186.8}{\joule\per\kilo\gram\per\kelvin}$. {\itshape Suggerimento: abbiamo visto in classe che Joule ha determinato l'equivalenza fra energia meccanica e calore; in questo caso, l'energia potenziale del masso viene trasformata in calore del masso, che verrà \textbf{ceduta} all'acqua. Quale deve essere il segno di questo calore? Scrivere l'equazione di bilancio del calore, supporre che durante il processo non venga disperso calore dal sistema acqua + masso all'esterno. Al termine dell'esercizio assicuratevi che la temperatura di equilibrio che avete trovato sia sensata, in particolare: la temperatura di equilibrio sarà maggiore o minore della temperatura di equilibrio dello stesso sistema nel quale però il masso non cade ma viene semplicemente adagiato nel laghetto ($E_\pd{pot}=0$)?}
\end{exc}
\end{document}
