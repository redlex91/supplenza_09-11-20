(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "a4paper" "oneside" "openany" "extrafontsizes")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontspec" "no-math") ("inputenc" "utf8") ("babel" "italian") ("hyperref" "hidelinks") ("xcolor" "svgnames")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "fontspec"
    "inputenc"
    "babel"
    "microtype"
    "hyperref"
    "xcolor"
    "tikz"
    "tabularx"
    "lipsum"
    "amsmath"
    "amsthm"
    "amssymb"
    "eulervm"
    "tkz-euclide")
   (TeX-add-symbols
    '("sol" 1))
   (LaTeX-add-xcolor-definecolors
    "ThemeBlue"
    "ThemeGray")
   (LaTeX-add-amsthm-newtheorems
    "ex")
   (LaTeX-add-amsthm-newtheoremstyles
    "easy"))
 :latex)

