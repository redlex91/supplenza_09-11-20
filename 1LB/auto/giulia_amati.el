(TeX-add-style-hook
 "giulia_amati"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontspec" "no-math") ("inputenc" "utf8") ("babel" "italian")))
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "fontspec"
    "inputenc"
    "babel"
    "microtype"
    "amsmath"
    "amsfonts"
    "tabularx"))
 :latex)

